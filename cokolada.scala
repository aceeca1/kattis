// Algorithm: None
object cokolada {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = Math.getExponent(n - 1) + 1
        val b = Math.getExponent(n & -n)
        println("%d %d".format(1 << a, a - b))
    }
}
