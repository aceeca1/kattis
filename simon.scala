// Algorithm: None
object simon {
    import io.StdIn._

    val simon = "simon says "

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val s = readLine
            if (s.startsWith(simon)) println(s.substring(simon.size))
            else println
        }
    }
}
