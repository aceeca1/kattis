object Snippet {
    import io.StdIn._
    import collection._
    import collection.mutable.Buffer

    val exts = Array(".scala", ".cpp")
    val reSnippet = "\\s*// Snippet: (\\w+).*".r

    def main(args: Array[String]) {
        val a = new mutable.HashMap[String, mutable.HashSet[String]]
        def put(dir: java.io.File) {
            for (i <- dir.listFiles) {
                if (i.isFile && exts.exists{ext => i.getName.endsWith(ext)}) {
                    Console.err.println("Processing: " + i.getName)
                    for (line <- io.Source.fromFile(i).getLines) line match {
                        case reSnippet(name) =>
                            a.getOrElseUpdate(name, new mutable.HashSet[String]).add(i.getName)
                        case _ => ()
                    }
                }
                if (i.isDirectory) put(i)
            }
        }
        put(new java.io.File("."))
        Console.err.println
        for ((k, v) <- a) {
            val vv = v.map{s =>
                if (!s.endsWith(exts(0))) s
                else s.substring(0, s.size - exts(0).size)
            }.mkString(", ")
            println(s"$k: $vv")
        }
    }
}
