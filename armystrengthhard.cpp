// Algorithm: None
#include <cstdio>
using namespace std;

int readMax(int z) {
    int ans = 0;
    for (int i = 0; i < z; ++i) {
        int k;
        scanf("%d", &k);
        if (k > ans) ans = k;
    }
    return ans;
}

int main() {
    int cases;
    scanf("%d", &cases);
    while (cases--) {
        int ng, nm;
        scanf("%d%d", &ng, &nm);
        int ag = readMax(ng);
        int am = readMax(nm);
        if (ag >= am) printf("Godzilla\n");
        else printf("MechaGodzilla\n");
    }
    return 0;
}
