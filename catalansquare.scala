// Algorithm: None
object catalansquare {
    import io.StdIn._

    def catalan(n: Int) = choose[BigInt](n + n, n) / (n + 1)

    def main(args: Array[String]) {
        val n = readInt
        println(catalan(n + 1))
    }

    // Snippet: choose
    def choose[T](n: Int, k: Int)(implicit integral: Integral[T]) = {
        import integral._
        var ans = one
        for (i <- 1 to k) ans = ans * fromInt(n + 1 - i) / fromInt(i)
        ans
    }
}
