// Algorithm: None
#include <cstdio>
#include <cstdint>
#include <cinttypes>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    vector<int64_t> a(n);
    for (int i = 0; i < n; ++i) scanf("%" SCNd64, &a[i]);
    sort(a.begin(), a.end());
    int64_t ans = 0;
    for (int i = a.size() - 3; i >= 0; i -= 3) ans += a[i];
    printf("%" PRId64 "\n", ans);
    return 0;
}
