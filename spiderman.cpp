// Algorithm: Deep First Search
#include <cstdio>
#include <vector>
#include <string>
#include <climits>
using namespace std;

struct Spider {
    vector<int> a, b;
    string ans, sb;
    int minHeight = INT_MAX;

    Spider(vector<int>&& a_): a(move(a_)), b(a.size() + 1) {
        for (int i = a.size() - 1; i >= 0; --i) b[i] = b[i + 1] + a[i];
    }

    void put(int height, int n) {
        if (sb.size() == a.size()) {
            if (!n) {
                ans = sb;
                minHeight = height;
            }
            return;
        }
        if (n >= a[sb.size()]) {
            sb += 'D';
            put(height, n - a[sb.size() - 1]);
            sb.pop_back();
        }
        int nNew = n + a[sb.size()];
        if (nNew < minHeight && nNew <= b[sb.size() + 1]) {
            sb += 'U';
            put(max(height, nNew), nNew);
            sb.pop_back();
        }
    }

    string calculate() {
        put(0, 0);
        if (!ans.size()) return "IMPOSSIBLE";
        return move(ans);
    }
};

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> a(n);
        int s = 0;
        for (int i = 0; i < n; ++i) {
            scanf("%d", &a[i]);
            s += a[i];
        }
        if (s & 1) { printf("IMPOSSIBLE\n"); continue; }
        s >>= 1;
        vector<bool> b(s + 1);
        b[0] = true;
        for (int i = 0; i < n; ++i)
            for (int j = s; j >= a[i]; --j)
                b[j] = b[j] | b[j - a[i]];
        if (!b[s]) { printf("IMPOSSIBLE\n"); continue; }
        printf("%s\n", Spider(move(a)).calculate().c_str());
    }
    return 0;
}
