// Algorithm: None
object modulo {
    import io.StdIn._

    def main(args: Array[String]) {
        var a = 0L
        for (i <- 1 to 10) a |= 1L << (readInt % 42)
        println(java.lang.Long.bitCount(a))
    }
}
