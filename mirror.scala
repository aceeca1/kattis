// Algorithm: None
object mirror {
    import io.StdIn._

    def main(args: Array[String]) {
        for (cases <- 1 to readInt) {
            val Array(r, c) = readLine.split(" ").map{_.toInt}
            val a = Array.fill(r){readLine}
            println(s"Test $cases")
            for (i <- a.reverseIterator) println(i.reverse)
        }
    }
}
