// Algorithm: None
object simpleaddition {
    import io.StdIn._

    def main(args: Array[String]) {
        val a1 = BigInt(readLine)
        val a2 = BigInt(readLine)
        println(a1 + a2)
    }
}
