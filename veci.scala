// Algorithm: None
object veci {
    import io.StdIn._

    def swap[T](a: Array[T], k1: Int, k2: Int) {
        val ak1 = a(k1)
        a(k1) = a(k2)
        a(k2) = ak1
    }

    def reverse[T](a: Array[T], k1: Int, k2: Int) {
        var p1 = k1
        var p2 = k2 - 1
        while (p1 < p2) {
            swap(a, p1, p2)
            p1 += 1
            p2 -= 1
        }
    }

    def main(args: Array[String]) {
        var a = readLine.toCharArray
        Range(a.size - 2, -1, -1).find{k => a(k) < a(k + 1)} match {
            case Some(k) =>
                val k1 = a.lastIndexWhere(_ > a(k))
                swap(a, k, k1)
                reverse(a, k + 1, a.size)
            case None => a = Array('0')
        }
        println(new String(a))
    }
}
