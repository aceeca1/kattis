// Algorithm: None
object busyschedule {
    import io.StdIn._

    class Time(val s: String) {
        val ss = s.split(" ")
        val ss0 = ss(0).split(":")
        val h = ss0(0).toInt
        val m = ss0(1).toInt
        val ap = ss(1)
        val not12 = !(h == 12)
        def toTuple = (ap, not12, h, m)
    }

    def main(args: Array[String]) {
        var head = true
        while (true) {
            val n = readInt
            if (n == 0) return
            if (!head) println
            head = false
            val a = Array.fill(n){new Time(readLine)}.sortBy{_.toTuple}
            for (i <- a) println(i.s)
        }
    }
}
