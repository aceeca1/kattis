// Algorithm: Shortest Path
object bigtruck {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}
        val g = Array.fill(n + 1){Buffer[(Int, Int)]()}
        for (_ <- 1 to readInt) {
            val Array(no1, no2, c) = readLine.split(" ").map{_.toInt}
            g(no1).append((no2, c * 10001 - a(no1 - 1)))
            g(no2).append((no1, c * 10001 - a(no2 - 1)))
        }
        new ShortPath[Int] {
            def sizeV = g.size
            def e(no: Int) = g(no).toIterator
            def src = 1
            def infinity = Int.MaxValue
        }.dis(n) match {
            case Int.MaxValue => println("impossible")
            case len =>
                val d1 = (len + 10000) / 10001
                val d2 = d1 * 10001 - len + a(n - 1)
                println(s"$d1 $d2")
        }
    }

    // Snippet: ShortPath
    abstract class ShortPath[T: reflect.ClassTag](implicit num: Numeric[T]) {
        import collection._
        import collection.mutable.Buffer
        import num._

        def sizeV: Int
        def e(no: Int): Iterator[(Int, T)]
        def src: Int
        def infinity: T

        val dis = Array.fill(sizeV){infinity}
        val from = Array.ofDim[Int](sizeV)

        {
            val q = new java.util.ArrayDeque[Int]
            val inQ = Array.ofDim[Boolean](sizeV)
            dis(src) = zero
            q.add(src)
            inQ(src) = true
            while (!q.isEmpty) {
                val qH = q.poll
                inQ(qH) = false
                for ((t, c) <- e(qH)) {
                    val newDis = dis(qH) + c
                    if (newDis < dis(t)) {
                        dis(t) = newDis
                        from(t) = qH
                        if (!inQ(t)) {
                            if (!q.isEmpty && newDis < dis(q.peek))
                                q.addFirst(t)
                            else
                                q.addLast(t)
                            inQ(t) = true
                        }
                    }
                }
            }
        }

        def route(dst: Int) = {
            val ans = Buffer[Int]()
            var no = dst
            while (no != src) {
                ans.append(no)
                no = from(no)
            }
            ans.append(src)
            ans.reverse
        }
    }
}
