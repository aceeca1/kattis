// Algorithm: None
object polygonarea {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val n = readInt
            if (n == 0) return
            val a = Array.fill(n){
                val line = readLine.split(" ").map{_.toDouble}
                Complex(line(0), line(1))
            }
            var ans = 0.0
            for (i <- a.indices) {
                val ai0 = a(i)
                val ai1 = if (i != n - 1) a(i + 1) else a(0)
                ans += ai0.cross(ai1)
            }
            if (ans >= 0.0) println("CCW %.1f".format(ans * 0.5))
            else println("CW %.1f".format(ans * -0.5))
        }
    }

    // Snippet: Complex
    case class Complex(x: Double, y: Double) {
        def cross(that: Complex) = {
            x * that.y - y * that.x
        }
    }
}
