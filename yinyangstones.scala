// Algorithm: None
object yinyangstones {
    import io.StdIn._

    def main(args: Array[String]) {
        val s = readLine
        if (s.count{_ == 'W'} << 1 == s.size) println(1)
        else println(0)
    }
}
