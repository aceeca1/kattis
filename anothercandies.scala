// Algorithm: None
object anothercandies {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            readLine
            val n = readInt
            val a = Iterator.fill(n){BigInt(readLong)}.sum
            if (a % n == 0) println("YES")
            else println("NO")
        }
    }
}
