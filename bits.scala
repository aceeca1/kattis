// Algorithm: None
object bits {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val s = readLine
            var a = 0
            var ans = 0
            for (i <- s) {
                a = a * 10 + (i - '0')
                val bc = Integer.bitCount(a)
                if (bc > ans) ans = bc
            }
            println(ans)
        }
    }
}
