// Algorithm: Binary Search
object closestsums {
    import io.StdIn._

    def main(args: Array[String]) {
        var cases = 1
        while (true) {
            val line = readLine
            if (line == null) return
            val n = line.toInt
            val a = Array.fill(n){readInt}.sorted
            println(s"Case $cases:")
            for (_ <- 1 to readInt) {
                val s = readInt
                var ans = Int.MaxValue
                var iAns = 0
                for (i <- a) {
                    val c = s - i
                    var k2 = bSearch(0, a.size){a(_) >= c}
                    var k1 = k2 - 1
                    if (k1 >= 0 && i == a(k1)) k1 -= 1
                    if (k2 < a.size && i == a(k2)) k2 += 1
                    if (k1 >= 0) {
                        val t = c - a(k1)
                        if (t < ans) { ans = t; iAns = a(k1) + i }
                    }
                    if (k2 < a.size) {
                        val t = a(k2) - c
                        if (t < ans) { ans = t; iAns = a(k2) + i }
                    }
                }
                println(s"Closest sum to $s is $iAns.")
            }
            cases += 1
        }
    }

    // Snippet: bSearch
    def bSearch(x1: Int, x2: Int)(f: Int => Boolean): Int = {
        if (x1 == x2) return x1
        val m = (x1 + x2) >> 1
        if (f(m)) bSearch(x1, m)(f) else bSearch(m + 1, x2)(f)
    }
}

