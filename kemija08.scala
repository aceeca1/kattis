// Algorithm: None
object kemija08 {
    import io.StdIn._

    def main(args: Array[String]) {
        val s = readLine
        val sb = new StringBuilder(s.size)
        var i = 0
        while (i < s.size) {
            sb.append(s(i))
            s(i) match {
                case 'a' | 'e' | 'i' | 'o' | 'u' => i += 3
                case _ => i += 1
            }
        }
        println(sb)
    }
}
