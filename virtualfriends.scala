// Algorithm: Union Find
object virtualfriends {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val g = new GraphV[String, Unit]
            val a = Array.fill(readInt){
                val Array(s1, s2) = readLine.split(" ")
                val no1 = g.addVertex(s1)
                val no2 = g.addVertex(s2)
                (no1, no2)
            }
            val uf = new UnionFind(g.e.size)
            for ((no1, no2) <- a) {
                val root = uf.union(no1, no2)
                println(uf.z(root))
            }
        }
    }

    // Snippet: GraphV
    class GraphV[V, E] {
        import collection._
        import collection.mutable.Buffer

        val v = new mutable.HashMap[V, Int]
        val e = Buffer[Buffer[E]]()

        def addVertex = {
            e.append(Buffer[E]())
            e.size - 1
        }

        def addVertex(label: V): Int = v.get(label) match {
            case Some(n) => n
            case None => v.update(label, e.size); addVertex
        }
    }

    // Snippet: UnionFind
    class UnionFind(n: Int) {
        val a = Array.range(0, n)
        val z = Array.fill(n){1}

        def find(no: Int) = {
            var root = no
            while (a(root) != root) root = a(root)
            var k = no
            while (a(k) != root) {
                val ak = a(k)
                a(k) = root
                k = ak
            }
            root
        }

        def union(no1: Int, no2: Int) = {
            val root1 = find(no1)
            val root2 = find(no2)
            if (root1 == root2) root1
            else if (z(root1) < z(root2)) {
                a(root1) = root2
                z(root2) += z(root1)
                root2
            } else {
                a(root2) = root1
                z(root1) += z(root2)
                root1
            }
        }
    }
}
