// Algorithm: None
object karte {
    import io.StdIn._
    import collection._

    def main(args: Array[String]) {
        val s = readLine
        val a = Array.fill(4){new mutable.HashSet[Int]}
        for (i <- Range(0, s.size, 3)) {
            val suit = s(i) match {
                case 'P' => 0
                case 'K' => 1
                case 'H' => 2
                case 'T' => 3
            }
            val num = s.substring(i + 1, i + 3).toInt
            if (!a(suit).add(num)) {
                println("GRESKA")
                return
            }
        }
        println(a.map{ai => 13 - ai.size}.mkString(" "))
    }
}
