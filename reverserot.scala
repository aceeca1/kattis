// Algorithm: None
object reverserot {
    import io.StdIn._

    val tr1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_."
    val tr2 = Array.ofDim[Int](256)
    for (i <- tr1.indices) tr2(tr1(i)) = i

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == "0") return
            val Array(rot, s) = line.split(" ")
            val rotI = rot.toInt
            println(s.reverseMap{i => tr1((tr2(i) + rotI) % tr1.size)})
        }
    }
}
