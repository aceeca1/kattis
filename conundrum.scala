// Algorithm: None
object conundrum {
    import io.StdIn._

    def main(args: Array[String]) {
        val s = readLine
        var ans = 0
        for (i <- s.indices) {
            val a = i % 3 match {
                case 0 => 'P'
                case 1 => 'E'
                case 2 => 'R'
            }
            if (a != s(i)) ans += 1
        }
        println(ans)
    }
}
