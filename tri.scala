// Algorithm: None
object tri {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(a, b, c) = readLine.split(" ").map{_.toInt}
        println(() match {
            case _ if a + b == c => s"$a+$b=$c"
            case _ if a * b == c => s"$a*$b=$c"
            case _ if a == b + c => s"$a=$b+$c"
            case _ if a == b - c => s"$a=$b-$c"
            case _ if a == b * c => s"$a=$b*$c"
            case _ if a * c == b => s"$a=$b/$c"
        })
    }
}
