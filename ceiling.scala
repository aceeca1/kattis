object ceiling {
    import io.StdIn._
    import collection._

    class TreeNode(val v: Int) {
        var ch0: TreeNode = null
        var ch1: TreeNode = null
        override def hashCode = {
            val a0 = if (ch0 == null) 0 else ch0.hashCode
            val a1 = if (ch1 == null) 0 else ch1.hashCode
            (a0, a1).hashCode
        }
    }

    class Tree {
        var root: TreeNode = null
        def insert(u: Int) {
            def walk(tr: TreeNode): TreeNode = () match {
                case _ if tr == null =>  new TreeNode(u)
                case _ if u < tr.v =>    tr.ch0 = walk(tr.ch0); tr
                case _ =>                tr.ch1 = walk(tr.ch1); tr
            }
            root = walk(root)
        }
    }

    def main(args: Array[String]) {
        val Array(n, k) = readLine.split(" ").map{_.toInt}
        val a = new mutable.HashSet[Int]
        for (i <- 1 to n) {
            var tree = new Tree
            for (i <- readLine.split(" ").map{_.toInt}) tree.insert(i)
            a.add(tree.root.hashCode)
        }
        println(a.size)
    }
}
