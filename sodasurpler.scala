// Algorithm: None
object sodasurpler {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(e, f, c) = readLine.split(" ").map{_.toInt}
        var emptyBottles = e + f
        var drunk = 0
        while (emptyBottles >= c) {
            val bottles = emptyBottles / c
            drunk += bottles
            emptyBottles = emptyBottles % c + bottles
        }
        println(drunk)
    }
}
