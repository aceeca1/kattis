// Algorithm: Extended Greatest Common Divisor
object robotopia {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(l1, a1, l2, a2, lt, at) =
                readLine.split(" ").map{_.toInt}
            val m0 = l1 * a2 - l2 * a1
            val m1 = lt * a2 - l2 * at
            val m2 = l1 * at - lt * a1
            if (m0 == 0)
                if (m1 == 0) {
                    val dio = new Diophantine(l1, l2, lt)
                    if (dio.hasSolution) {
                        val lb = divUpper(1 - dio.x0, dio.b / dio.d)
                        val ub = divLower(dio.y0 - 1, dio.a / dio.d)
                        if (lb == ub) {
                            val x = dio.x0 + dio.b / dio.d * lb
                            val y = dio.y0 - dio.a / dio.d * lb
                            println(s"$x $y")
                        } else println("?")
                    } else println("?")
                } else println("?")
            else {
                val x = m1 / m0
                val y = m2 / m0
                def eq1 = l1 * x + l2 * y == lt
                def eq2 = a1 * x + a2 * y == at
                if (x >= 1 && y >= 1 && eq1 && eq2) println(s"$x $y")
                else println("?")
            }
        }
    }

    // Snippet: divLower
    def divLower(a: Int, b: Int): Int =
        if (b < 0) divLower(-a, -b)
        else if (a < 0) -divUpper(-a, b)
        else a / b

    def divUpper(a: Int, b: Int): Int =
        if (b < 0) divUpper(-a, -b)
        else if (a < 0) -divLower(-a, b)
        else (a + b - 1) / b

    // Snippet: Bezout
    class Bezout(val a: Int, val b: Int) {
        def solve(a: Int, b: Int, m: Int, n: Int): (Int, Int) =
            if (b == 0) (a, m) else solve(b, a % b, n, m - a / b * n)
        val (d, x) = solve(a, b, 1, 0)
        val y = (d - a * x) / b
    }

    // Snippet: Diophantine
    class Diophantine(val a: Int, val b: Int, val c: Int) {
        val bz = new Bezout(a, b)
        val (hasSolution, d, x0, y0) = if (c % bz.d == 0) {
            val k = c / bz.d
            (true, bz.d, bz.x * k, bz.y * k)
        } else (false, 0, 0, 0)
    }
}
