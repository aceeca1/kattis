// Algorithm: None
object symmetricorder {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        var cases = 1
        while (true) {
            val n = readInt
            if (n == 0) return
            println(s"SET $cases")
            val a1 = Buffer[String]()
            val a2 = Buffer[String]()
            for (i <- 0 until n) {
                if ((i & 1) == 0) a1.append(readLine)
                else a2.append(readLine)
            }
            for (i <- a1) println(i)
            for (i <- a2.reverseIterator) println(i)
            cases += 1
        }
    }
}
