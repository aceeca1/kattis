// Algorithm: None
object growlinggears {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readInt
            val u = Array.fill(n){
                val Array(a, b, c) = readLine.split(" ").map{_.toInt}
                c + b * b / (a * 4.0)
            }
            println(u.indices.maxBy(u) + 1)
        }
    }
}
