// Algorithm: None
object t9spelling {
    import io.StdIn._

    val a1 = Array(
        " ", "", "abc", "def",
        "ghi", "jkl", "mno",
        "pqrs", "tuv", "wxyz")
    val a2 = Array.ofDim[Int](256)
    val a3 = Array.ofDim[Int](256)
    for (i <- a1.indices)
        for (j <- a1(i).indices) {
            a2(a1(i)(j)) = i
            a3(a1(i)(j)) = j + 1
        }

    def main(args: Array[String]) {
        for (cases <- 1 to readInt) {
            val sb = new StringBuilder
            var last = -1
            for (i <- readLine) {
                if (a2(i) == last) sb.append(' ')
                for (j <- 1 to a3(i)) sb.append(a2(i))
                last = a2(i)
            }
            println(s"Case #$cases: $sb")
        }
    }
}
