// Algorithm: None
object mosquito {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            var Array(m, p, l, e, r, s, n) = line.split(" ").map{_.toInt}
            for (i <- 0 until n) {
                val m0 = m
                m = p / s
                p = l / r
                l = m0 * e
            }
            println(m)
        }
    }
}
