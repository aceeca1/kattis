// Algorithm: None
object bestcompression {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, b) = readLine.split(" ").map{_.toLong}
        if (n < (1 << (b + 1))) println("yes") else println("no")
    }
}
