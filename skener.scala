// Algorithm: None
object skener {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(r, c, zr, zc) = readLine.split(" ").map{_.toInt}
        for (_ <- 0 until r) {
            val s = readLine
            val sb = new StringBuilder
            for (i <- s) for (j <- 0 until zc) sb.append(i)
            for (i <- 0 until zr) println(sb)
        }
    }
}
