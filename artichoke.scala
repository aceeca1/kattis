// Algorithm: None
object artichoke {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(p, a, b, c, d, n) = readLine.split(" ").map{_.toInt}
        def price(k: Int) =
            p * (Math.sin(a * k + b) + Math.cos(c * k + d) + 2.0)
        var pm = Double.NegativeInfinity
        var ans = 0.0
        for (i <- 1 to n) {
            val pi = price(i)
            val u = pm - pi
            if (u > ans) ans = u
            if (pi > pm) pm = pi
        }
        println(ans)
    }
}
