// Algorithm: None
object timebomb {
    import io.StdIn._
    import collection._

    val numS = Array(
        "***   * *** *** * * *** *** *** *** ***",
        "* *   *   *   * * * *   *     * * * * *",
        "* *   * *** *** *** *** ***   * *** ***",
        "* *   * *     *   *   * * *   * * *   *",
        "***   * *** ***   * *** ***   * *** ***")

    def readNum(num: Array[String], k: Int) = {
        val ret = new StringBuilder
        for (i <- (k << 2) to (k << 2) + 2)
            for (j <- 0 to 4) ret.append(num(j)(i))
        ret.toString
    }

    val ocr = new mutable.HashMap[String, Int]
    for (i <- 0 to 9) ocr.update(readNum(numS, i), i)

    def main(args: Array[String]) {
        val num = Array.fill(5){readLine}
        val n = (num(0).size + 1) >>> 2
        val ans = try {
            (0 until n).map{i => ocr(readNum(num, i))}.reduce(_ * 10 + _)
        } catch {
            case _: NoSuchElementException => -1
        }
        if (ans % 6 == 0) println("BEER!!")
        else println("BOOM!!")
    }
}
