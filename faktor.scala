// Algorithm: None
object faktor {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(a, i) = readLine.split(' ').map{_.toInt}
        println(a * (i - 1) + 1)
    }
}
