// Algorithm: Binary Search
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

struct IsPossible {
    int n, k;
    vector<int> a;

    bool operator()(int d) const {
        int i = 0;
        for (int u = 0; u < n; ++u) {
            int ub = u * k << 1;
            while (i <= ub) {
                if (a[i + 1] - a[i] <= d) break;
                ++i;
            }
            if (i <= ub) i += 2;
            else return false;
        }
        return true;
    }
};

template<class Func>
int bSearch(int x1, int x2, Func&& f) {
    while (x1 != x2) {
        int m = (x1 + x2) >> 1;
        if (f(m)) x2 = m;
        else x1 = m + 1;
    }
    return x1;
}

int main() {
    IsPossible mc;
    scanf("%d%d", &mc.n, &mc.k);
    int z = mc.n * mc.k << 1;
    mc.a.resize(z);
    for (int i = 0; i < z; ++i) scanf("%d", &mc.a[i]);
    sort(mc.a.begin(), mc.a.end());
    printf("%d\n", bSearch(0, mc.a.back(), mc));
    return 0;
}
