// Algorithm: None
object natrij {
    import io.StdIn._

    class Time(val s: Int) {
        override def toString = {
            var c = s
            val second = c % 60
            c /= 60
            val minute = c % 60
            c /= 60
            val hour = c
            "%02d:%02d:%02d".format(hour, minute, second)
        }
    }

    def parseTime(s: String) =
        new Time(s.split(":").map{_.toInt}.reduce{_ * 60 + _})

    def main(args: Array[String]) {
        val time1 = parseTime(readLine)
        val time2 = parseTime(readLine)
        val diff = (time2.s - time1.s + (86400 + 86400 - 1)) % 86400 + 1
        println(new Time(diff))
    }
}
