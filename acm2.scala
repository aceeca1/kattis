// Algorithm: None
object acm2 {
    import io.StdIn._

    def compete(a: Array[Int]): (Int, Int) = {
        var numAC = 0
        var penalty = 0
        var time = 0
        for (i <- a) {
            time += i
            if (time > 300) return (numAC, penalty)
            numAC += 1
            penalty += time
        }
        return (numAC, penalty)
    }

    def main(args: Array[String]) {
        val Array(n, p) = readLine.split(" ").map{_.toInt}
        val a = readLine.split(" ").map{_.toInt}
        val a0 = a(0)
        a(0) = a(p)
        a(p) = a0
        java.util.Arrays.sort(a, 1, a.size)
        val (numAC, penalty) = compete(a)
        println(s"$numAC $penalty")
    }
}
