// Algorithm: None
object zoo {
    import io.StdIn._

    def main(args: Array[String]) {
        var caseNo = 1
        while (true) {
            val n = readInt
            if (n == 0) return
            val a = Array.fill(n){
                readLine.split(" ").last.toLowerCase
            }.sorted
            println(s"List $caseNo:")
            new PartitionBy[String, String] {
                def in = a
                def eqv(a1: String, a2: String) = a1 == a2
                def mapTo = new Aggregator[String, String] {
                    var value: String = null
                    var amount = 0
                    def add(data: String) {
                        value = data
                        amount += 1
                    }
                    def result = s"$value | $amount"
                }
            }.foreach(println)
            caseNo += 1
        }
    }

    // Snippet: PartitionBy
    abstract class Aggregator[T, U] {
        def add(data: T): Unit
        def result: U
    }

    abstract class PartitionBy[T, U] extends Traversable[U] {
        def in: Traversable[T]
        def eqv(a1: T, a2: T): Boolean
        def mapTo: Aggregator[T, U]

        def foreach[V](f: U => V) {
            var data = null.asInstanceOf[T]
            var aggr: Aggregator[T, U] = null
            for (i <- in)
                if (aggr == null) {
                    data = i
                    aggr = mapTo
                    aggr.add(i)
                } else if (eqv(data, i)) {
                    data = i
                    aggr.add(i)
                } else {
                    f(aggr.result)
                    data = i
                    aggr = mapTo
                    aggr.add(i)
                }
            if (aggr != null) f(aggr.result)
        }
    }
}
