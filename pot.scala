// Algorithm: None
object pot {
    import io.StdIn._

    def main(args:Array[String]) {
        var ans = 0
        for (i <- 1 to readInt) {
            val s = readLine
            val a = s.substring(0, s.size - 1).toInt
            val b = s(s.size - 1) - '0'
            ans += Iterator.fill(b)(a).product
        }
        println(ans)
    }
}
