// Algorithm: None
object ladder {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(h, v) = readLine.split(" ").map{_.toInt}
        val ans = Math.ceil(h / Math.sin(v / 180.0 * Math.PI))
        println("%.0f".format(ans))
    }
}
