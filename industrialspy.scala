// Algorithm: Deep First Search
object industrialspy {
    import io.StdIn._
    import collection._

    val prime = new Prime(10000000)

    class PrimePerm(s: String) {
        val a = s.toCharArray
        var ans = mutable.HashSet[Int]()

        def swap(a: Array[Char], i1: Int, i2: Int) {
            val ai1 = a(i1)
            a(i1) = a(i2)
            a(i2) = ai1
        }

        def put(m: Int, n: Int) {
            if (prime.is(n)) ans.add(n)
            for (i <- m until a.size) {
                swap(a, m, i)
                put(m + 1, n * 10 + (a(m) - '0'))
                swap(a, m, i)
            }
        }
    }

    def main(args: Array[String]) {
        for (_ <- 0 until readInt) {
            val pp = new PrimePerm(readLine)
            pp.put(0, 0)
            println(pp.ans.size)
        }
    }

    // Snippet: Prime
    class Prime(n: Int) {
        import collection.mutable.Buffer

        val a = Array.ofDim[Int](n + 1)

        {
            var u = 0
            for (i <- 2 to n) {
                var v = a(i)
                if (v == 0) { v = i; a(u) = i; u = i }
                var w = 2
                while (w > 0 && i * w <= n) {
                    a(i * w) = w
                    if (w >= v) w = -1 else w = a(w)
                }
            }
            a(u) = n + 1
        }

        def is(n: Int) = n >= 2 && a(n) > n
        def next(n: Int) = a(n)

        def toBuffer = {
            val ret = Buffer[Int]()
            var i = 2
            while (i <= n) {
                ret.append(i)
                i = a(i)
            }
            ret
        }
    }
}
