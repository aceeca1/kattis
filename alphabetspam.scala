// Algorithm: None
object alphabetspam {
    import io.StdIn._

    def main(args: Array[String]) {
        val s = readLine
        var white = 0
        var lower = 0
        var upper = 0
        var symbol = 0
        for (i <- s)
            if (i == '_') white += 1
            else if (i.isLower) lower += 1
            else if (i.isUpper) upper += 1
            else symbol += 1
        println(white.toDouble / s.size)
        println(lower.toDouble / s.size)
        println(upper.toDouble / s.size)
        println(symbol.toDouble / s.size)
    }
}
