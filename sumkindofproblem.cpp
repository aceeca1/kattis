// Algorithm: None
#include <cstdio>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int k, n;
        scanf("%d%d", &k, &n);
        int s1 = n * (n + 1) >> 1;
        int s2 = n * n;
        int s3 = n * (n + 1);
        printf("%d %d %d %d\n", k, s1, s2, s3);
    }
    return 0;
}
