// Algorithm: None
object tutorial {
    import io.StdIn._

    def f(n: Double, t: Int) = t match {
        case 1 =>
            if (n > 100) Double.PositiveInfinity
            else (2.0 to n by 1.0).product
        case 2 => Math.pow(2.0, n)
        case 3 => (n * n) * (n * n)
        case 4 => n * n * n
        case 5 => n * n
        case 6 => n * Math.log(n) / Math.log(2.0)
        case 7 => n
    }

    def main(args: Array[String]) {
        val Array(m, n, t) = readLine.split(" ").map{_.toInt}
        if (f(n, t) > m + 1e-6) println("TLE") else println("AC")
    }
}
