// Algorithm: Union Find
#include <cstdio>
#include <vector>
using namespace std;

// Snippet: UnionFind
struct UnionFind {
    vector<int> a, z;

    UnionFind(int n): a(n), z(n, 1) {
        for (int i = 0; i < n; ++i) a[i] = i;
    }

    int find(int no) {
        int root = no;
        while (a[root] != root) root = a[root];
        int k = no;
        while (a[k] != root) {
            int ak = a[k];
            a[k] = root;
            k = ak;
        }
        return root;
    }

    int union0(int no1, int no2) {
        int root1 = find(no1);
        int root2 = find(no2);
        if (root1 == root2) return root1;
        if (z[root1] < z[root2]) {
            a[root1] = root2;
            z[root2] += z[root1];
            return root2;
        } else {
            a[root2] = root1;
            z[root1] += z[root2];
            return root1;
        }
    }
};

int main() {
    int n, q;
    scanf("%d%d", &n, &q);
    UnionFind uf(n);
    for (int i = 0; i < q; ++i) {
        char op;
        int a1, a2;
        scanf(" %c%d%d", &op, &a1, &a2);
        switch (op) {
            case '?':
                if (uf.find(a1) == uf.find(a2)) printf("yes\n");
                else printf("no\n"); break;
            case '=': uf.union0(a1, a2);
        }
    }
    return 0;
}
