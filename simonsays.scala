// Algorithm: None
object simonsays {
    import io.StdIn._

    val simon = "Simon says"

    def main(args: Array[String]) {
        for (i <- 1 to readInt) {
            val s = readLine
            if (s.startsWith(simon)) println(s.substring(simon.size))
        }
    }
}
