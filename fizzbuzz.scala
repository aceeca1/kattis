// Algorithm: None
object fizzbuzz {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(x, y, n) = readLine.split(" ").map{_.toInt}
        for (i <- 1 to n) {
            val fizz = i % x == 0
            val buzz = i % y == 0
            println(if (fizz) {
                if (buzz) "FizzBuzz" else "Fizz"
            } else {
                if (buzz) "Buzz" else i.toString
            })
        }
    }
}
