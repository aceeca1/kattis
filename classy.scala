// Algorithm: None
object classy {
    import io.StdIn._

    class People(val name: String, val desc: String) extends Ordered[People] {
        def compare(that: People): Int = {
            val len = desc.size max that.desc.size
            for (i <- 0 until len) {
                val c1 = if (i < desc.size)       desc(i)       else '2'
                val c2 = if (i < that.desc.size)  that.desc(i)  else '2'
                if (c1 < c2) return 1
                if (c2 < c1) return -1
            }
            return name compare that.name
        }
    }

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val a = Array.fill(readInt){
                val line = readLine.split(" ")
                val name = line(0).init
                val describe = new String(line(1).split("-").reverseMap{
                    case "upper" => '3'
                    case "middle" => '2'
                    case "lower" => '1'
                })
                new People(name, describe)
            }.sorted
            for (i <- a) println(i.name)
            println("==============================")
        }
    }
}
