// Algorithm: None
object anagramcounting {
    import io.StdIn._

    def factorial(n: Int) = BigInt(2).to(BigInt(n)).product

    def main(args: Array[String]) {
        while (true) {
            val s = readLine
            if (s == null) return
            val a = Array.ofDim[Int](256)
            for (i <- s) a(i) += 1
            val a1 = factorial(s.size)
            val a2 = a.map(factorial).product
            println(a1 / a2)
        }
    }
}
