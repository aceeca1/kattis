// Algorithm: None
object mixedfractions {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val Array(n, k) = readLine.split(" ").map{_.toInt}
            if (n == 0) return
            println("%d %d / %d".format(n / k, n % k, k))
        }
    }
}
