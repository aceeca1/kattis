// Algorithm: None
object temperature {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(x, y) = readLine.split(" ").map{_.toInt}
        1 - y match {
            case 0 =>
                if (x == 0) println("ALL GOOD") else println("IMPOSSIBLE")
            case k =>
                println(x.toDouble / k)
        }
    }
}
