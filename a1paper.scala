// Algorithm: Math
object a1paper {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}
        var need = 1
        for (i <- 0 until a.size) {
            need += need
            if (a(i) < need) need -= a(i)
            else { a(i) = need; need = 0 }
        }
        if (need != 0) println("impossible")
        else println((0 until a.size).map{i =>
            val c1 = Math.pow(2.0, (i * 2.0 + 5.0) / -4.0)
            val c2 = Math.pow(2.0, (i * 4.0 + 7.0) / -4.0)
            val c = (c1 - c2) / (Math.sqrt(2.0) - 1.0)
            a(i) * c
        }.sum)
    }
}
