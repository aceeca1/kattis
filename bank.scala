// Algorithm: Greedy
object bank {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, t) = readLine.split(" ").map{_.toInt}
        val a = Array.fill(n){
            readLine.split(" ").map{_.toInt}
        }.sortBy{ai => -ai(0)}
        val b = Array.ofDim[Boolean](t)
        var ans = 0
        for (i <- a) {
            var k = i(1)
            while (k >= 0 && b(k)) k -= 1
            if (k >= 0) {
                b(k) = true
                ans += i(0)
            }
        }
        println(ans)
    }
}
