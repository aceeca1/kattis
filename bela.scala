// Algorithm: None
object bela {
    import io.StdIn._

    val num = "AKQJT987"
    val dom = Array(11, 4, 3, 20, 10, 14, 0, 0)
    val ndm = Array(11, 4, 3, 2, 10, 0, 0, 0)
    val numR = Array.ofDim[Int](256)
    for (i <- num.indices) numR(num(i)) = i

    def main(args: Array[String]) {
        val Array(n, b) = readLine.split(" ")
        val nI = n.toInt
        val bC = b(0)
        var ans = 0
        for (i <- 1 to nI << 2) {
            val s = readLine
            ans += (if (s(1) == bC) dom(numR(s(0))) else ndm(numR(s(0))))
        }
        println(ans)
    }
}
