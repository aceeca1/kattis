// Algorithm: Dynamic Programming
object rijeci {
    import io.StdIn._

    def main(args: Array[String]) {
        var a = 1L
        var b = 0L
        for (i <- 1 to readInt) {
            var a0 = a
            a = b
            b = a0 + b
        }
        println("%d %d".format(a, b))
    }
}
