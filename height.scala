// Algorithm: Segment Tree
object height {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val line = readLine.split(" ").map{_.toInt}
            val no = line(0)
            val a = line.drop(1)
            val b = Array.range(0, a.size).sortBy(a)
            val st = new STSum(b.size)
            var ans = 0
            for (i <- b.reverseIterator) ans += st.fetch(i, 1)
            println(s"$no $ans")
        }
    }

    // Snippet: STSum
    class STSum(n: Int) {
        val m = 1 << (Math.getExponent(n + 1) + 1)
        val a = Array.ofDim[Int](m + m)

        def fetch(pos: Int, d: Int) = {
            var p = pos + m
            var ans = 0
            while (p != 1) {
                if ((p & 1) != 0) ans += a(p - 1)
                a(p) += d
                p >>>= 1
            }
            ans
        }
    }
}
