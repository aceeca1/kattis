// Algorithm: Parsing
object v4thought {
    import io.StdIn._
    import collection._

    class Parse(s: String) {
        var i = 0

        def expr: Int = {
            var ret = term
            while (true) () match {
                case _ if i < s.size && s(i) == '+' =>
                    i += 1
                    ret += term
                case _ if i < s.size && s(i) == '-' =>
                    i += 1
                    ret -= term
                case _ => return ret
            }; ret
        }

        def term: Int = {
            var ret = 4
            while (true) () match {
                case _ if i < s.size && s(i) == '*' =>
                    i += 1
                    ret *= 4
                case _ if i < s.size && s(i) == '/' =>
                    i += 1
                    ret /= 4
                case _ => return ret
            }; ret
        }
    }

    val dict = {
        val ret = new mutable.HashMap[Int, String]
        for {
            i1 <- "+-*/"
            i2 <- "+-*/"
            i3 <- "+-*/"
            s = (new StringBuilder).append(i1).append(i2).append(i3).toString
        } ret.update(new Parse(s).expr, s)
        ret
    }

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readInt
            dict.get(n) match {
                case Some(m) => println(
                    "4 %c 4 %c 4 %c 4 = %d"
                    .format(m(0), m(1), m(2), n))
                case None => println("no solution")
            }
        }
    }
}

