// Algorithm: None
object yoda {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readLine
        val m = readLine
        val nb = new StringBuilder
        val mb = new StringBuilder
        for (i <- 0 until (n.size max m.size)) {
            val ni = if (i < n.size) n(n.size - 1 - i) else '0'
            val mi = if (i < m.size) m(m.size - 1 - i) else '0'
            if (ni > mi) nb.append(ni)
            else if (mi > ni) mb.append(mi)
            else { nb.append(ni); mb.append(mi) }
        }
        println(if (nb.isEmpty) "YODA" else nb.reverse.toInt)
        println(if (mb.isEmpty) "YODA" else mb.reverse.toInt)
    }
}
