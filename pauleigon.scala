// Algorithm: None
object pauleigon {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, p, q) = readLine.split(" ").map{_.toInt}
        if ((p + q) % (n + n) < n) println("paul")
        else println("opponent")
    }
}
