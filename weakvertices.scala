// Algorithm: None
object weakvertices {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val n = readInt
            if (n == -1) return
            val a = Array.fill(n){readLine.split(" ").map{_.toInt}}
            println((0 until n).filter{i =>
                val cross = for {
                    j1 <- 0 until n
                    if (a(i)(j1) != 0)
                    j2 <- 0 until n
                    if (a(i)(j2) != 0)
                } yield a(j1)(j2)
                cross.forall(_ == 0)
            }.mkString(" "))
        }
    }
}
