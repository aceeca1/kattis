// Algorithm: None
object hittingtargets {
    import io.StdIn._

    abstract class Figure {
        def has(x: Int, y: Int): Boolean
    }

    class Rect(
        val x1: Int, val y1: Int, val x2: Int, val y2: Int
    ) extends Figure {
        override def has(x: Int, y: Int) = {
            def inX = x1 <= x && x <= x2
            def inY = y1 <= y && y <= y2
            inX && inY
        }
    }

    class Circle(
        val cx: Int, val cy: Int, val r: Int
    ) extends Figure {
        override def has(x: Int, y: Int) = {
            def sqr(n: Int) = n * n
            sqr(x - cx) + sqr(y - cy) <= sqr(r)
        }
    }

    def readFigure = {
        val line = readLine.split(" ")
        line(0) match {
            case "rectangle" => new Rect(
                line(1).toInt, line(2).toInt,
                line(3).toInt, line(4).toInt
            )
            case "circle" => new Circle(
                line(1).toInt, line(2).toInt, line(3).toInt
            )
        }
    }

    def main(args: Array[String]) {
        val a = Array.fill(readInt){readFigure}
        for (i <- 1 to readInt) {
            val Array(x, y) = readLine.split(" ").map{_.toInt}
            println(a.count{_.has(x, y)})
        }
    }
}
