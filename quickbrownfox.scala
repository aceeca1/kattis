// Algorithm: None
object quickbrownfox {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            var a = 0
            for (i <- readLine) {
                val c = i.toLower
                if (c.isLower) a |= 1 << (c - 'a')
            }
            val ans = new StringBuilder(26)
            for (i <- 0 until 26)
                if (((a >>> i) & 1) == 0)
                    ans.append(('a' + i).toChar)
            if (ans.size == 0) println("pangram")
            else println(s"missing $ans")
        }
    }
}
