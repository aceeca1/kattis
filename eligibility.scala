// Algorithm: None
object eligibility {
    import io.StdIn._

    def isEligible(studies: Int, birth: Int, courses: Int): String = {
        if (studies >= 2010 || birth >= 1991) return "eligible"
        if (courses > 40) return "ineligible"
        return "coach petitions"
    }

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val s = readLine.split(" ")
            val name = s(0)
            val studies = s(1).split("/")(0).toInt
            val birth = s(2).split("/")(0).toInt
            val courses = s(3).toInt
            val ans = isEligible(studies, birth, courses)
            println("%s %s".format(name, ans))
        }
    }
}
