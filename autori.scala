// Algorithm: None
object autori {
    import io.StdIn._

    def main(args: Array[String]) {
        println(readLine.split("-").map{_.head}.mkString)
    }
}
