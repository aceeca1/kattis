// Algorithm: None
object bijele {
    import io.StdIn._

    val correctBijele = Array(1, 1, 2, 2, 2, 8)

    def main(args: Array[String]) {
        val a = readLine.split(" ").map{_.toInt}
        val b = Array.tabulate(a.size){i => correctBijele(i) - a(i)}
        println(b.mkString(" "))
    }
}
