// Algorithm: None
object permutationencryption {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val a = readLine.split(" ").map{_.toInt}.drop(1).map{_ - 1}
            if (a.size == 0) return
            val s = readLine
            val sb = new StringBuilder
            sb.append("'")
            for (i <- 0 until (s.size + a.size - 1) / a.size) {
                for (j <- 0 until a.size) {
                    val idx = i * a.size + a(j)
                    sb.append(if (idx >= s.size) ' ' else s(idx))
                }
            }
            sb.append("'")
            println(sb)
        }
    }
}
