// Algorithm: None
object plantingtrees {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}.sorted
        for (i <- a.indices) a(i) = a(i) + a.size - i
        println(a.max + 1)
    }
}
