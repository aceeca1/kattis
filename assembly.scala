// Algorithm: Find Cycle in Graph
object assembly {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val g = Array.ofDim[Boolean](52, 52)
        def vNo(s: String) = s(1) match {
            case '+' => ((s(0) - 'A') << 1)
            case '-' => ((s(0) - 'A') << 1) + 1
            case '0' => -1
        }
        for (ai <- readLine.split(" ")) {
            val u = Array.tabulate(4){
                k => vNo(ai.substring(k + k, k + k + 2))
            }.filter{_ != -1}
            for (i1 <- u.indices)
                for (i2 <- u.indices) if (i1 != i2)
                    g(u(i1) ^ 1)(u(i2)) = true
        }
        if (new HasCycle {
            def sizeV = 52
            def e(no: Int) = Iterator.range(0, sizeV).filter(g(no))
        }.ans) println("unbounded")
        else println("bounded")
    }

    // Snippet: HasCycle
    abstract class HasCycle {
        import collection._
        import collection.mutable.Buffer

        def sizeV: Int
        def e(no: Int): Iterator[Int]

        val status = Array.ofDim[Int](sizeV)
        var ans = false

        def visit(no: Int) {
            if (status(no) == 1) { ans = true; return }
            if (status(no) == 2) { return }
            status(no) = 1
            for (i <- e(no)) visit(i)
            status(no) = 2
        }

        for (i <- 0 until sizeV) visit(i)
    }
}
