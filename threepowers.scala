// Algorithm: None
object threepowers {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val n = BigInt(readLine)
            if (n == 0) return
            val sb = new StringBuilder
            sb.append("{ ")
            var k = n - 1
            var a = BigInt(1)
            while (k != 0) {
                if ((k & 1) != 0) {
                    if (sb.size != 2) sb.append(", ")
                    sb.append(a)
                }
                a *= 3
                k >>= 1
            }
            if (sb.size != 2) sb.append(" ")
            sb.append("}")
            println(sb)
        }
    }
}
