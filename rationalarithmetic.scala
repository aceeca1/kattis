// Algorithm: None
object rationalarithmetic {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val line = readLine.split(" ")
            val a1 = new Rational(line(0).toLong, line(1).toLong)
            val a2 = new Rational(line(3).toLong, line(4).toLong)
            line(2) match {
                case "+" => println(a1 + a2)
                case "-" => println(a1 - a2)
                case "*" => println(a1 * a2)
                case "/" => println(a1 / a2)
            }
        }
    }

    // Snippet: gcd
    def gcd[T](a1: T, a2: T)(implicit integral: Integral[T]): T = {
        import integral._
        if (a2 == zero) a1 else gcd(a2, a1 % a2)
    }

    // Snippet: Rational
    class Rational(val n: Long, val d: Long) {
        def simplify = {
            val g = Math.abs(gcd[Long](n, d))
            val g1 = if (d < 0) -g else g
            new Rational(n / g1, d / g1)
        }

        def +(that: Rational) =
            new Rational(n * that.d + that.n * d, d * that.d).simplify

        def -(that: Rational) =
            new Rational(n * that.d - that.n * d, d * that.d).simplify

        def *(that: Rational) =
            new Rational(n * that.n, d * that.d).simplify

        def /(that: Rational) =
            new Rational(n * that.d, d * that.n).simplify

        override def toString = "%d / %d".format(n, d)
    }
}
