// Algorithm: None
#include <cstdio>
#include <vector>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int n;
        scanf("%d", &n);
        vector<int> b(n);
        for (int i = 0; i < n; ++i) {
            int ai;
            scanf("%d", &ai);
            b[ai - 1] = i;
        }
        int k = 0;
        while (k + 1 < n && b[k] < b[k + 1]) ++k;
        printf("%d\n", n - (k + 1));
    }
    return 0;
}
