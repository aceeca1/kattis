// Algorithm: None
object skocimis {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(a, b, c) = readLine.split(" ").map{_.toInt}
        println((b - a max c - b) - 1)
    }
}
