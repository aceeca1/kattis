// Algorithm: None
object parking {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = 0 +: readLine.split(" ").map{_.toInt}
        for (i <- 0 to 3) a(i) *= i
        val b = Array.fill(3){readLine.split(" ").map{_.toInt}}
        val m = b.map{_(1)}.max
        val c = Array.ofDim[Int](m)
        for (Array(s, t) <- b)
            for (i <- s until t) c(i) += 1
        println(c.map(a).sum)
    }
}
