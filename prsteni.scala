// Algorithm: None
object prsteni {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}
        for (i <- 1 until n) {
            val d = gcd[Int](a(0), a(i))
            val b1 = a(0) / d
            val b2 = a(i) / d
            println(s"$b1/$b2")
        }
    }

    // Snippet: gcd
    def gcd[T](a1: T, a2: T)(implicit integral: Integral[T]): T = {
        import integral._
        if (a2 == zero) a1 else gcd(a2, a1 % a2)
    }
}
