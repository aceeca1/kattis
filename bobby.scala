// Algorithm: Dynamic Programming
object bobby {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(r, s, x, y, w) = readLine.split(" ").map{_.toInt}
            val win = s - r + 1L
            val lose = r - 1L
            var a = Array.ofDim[Long](x + 1)
            a(0) = 1L
            for (i <- 1 to y) {
                for (j <- Range(x, 0, -1))
                    a(j) = win * a(j - 1) + lose * a(j)
                a(0) *= s
            }
            if (w * a(x) > a(0)) println("yes")
            else println("no")
        }
    }
}
