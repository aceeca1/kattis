// Algorithm: Minimum Spanning Tree
#include <cstdio>
#include <vector>
#include <limits>
#include <cmath>
using namespace std;

// Snippet: Graph
template <class Edge>
struct Graph {
    vector<vector<Edge>> v;

    Graph(int n = 0): v(n) {}

    int sizeV() const { return v.size(); }

    template <class Lambda>
    void forE(int no, Lambda&& f) const {
        for (const Edge& e: v[no]) { f(e); }
    }
};

// Snippet: MST
template <class MSTQueue, class Edge, class Number>
struct MST {
    const Graph<Edge>& g;
    vector<int> parent;
    Number total;

    MST(const Graph<Edge>& g_): g(g_), parent(g.sizeV(), -1), total(0) {
        MSTQueue q(g.v.size());
        q.enqueue(-1, 0, Number(0));
        for (;;) {
            int s, t;
            Number c;
            q.dequeue(s, t, c);
            switch (s) {
                case -2: total = numeric_limits<Number>::max();
                case -3: return;
            }
            parent[t] = s;
            total += c;
            g.forE(t, [&](const Edge& e){ q.enqueue(t, e.t, e.c); });
        }
    }
};

// Snippet: MSTQueueDense
template <class Number>
struct MSTQueueDense {
    int n;
    vector<int> parent;
    vector<Number> cost;

    MSTQueueDense(int n_): n(n_), parent(n, -2),
        cost(n, numeric_limits<Number>::max()) {}

    void enqueue(int s, int t, const Number& c) {
        if (parent[t] == -3) return;
        if (c < cost[t]) { cost[t] = c; parent[t] = s; }
    }

    void dequeue(int& s, int& t, Number& c) {
        s = -3;
        c = numeric_limits<Number>::max();
        for (int i = 0; i < n; ++i)
            if (parent[i] != -3 && cost[i] < c) {
                c = cost[i];
                s = parent[i];
                t = i;
            }
        parent[t] = -3;
    }
};

void visit(Graph<int>& tree, int no) {
    printf("%d\n", no);
    tree.forE(no, [&](int t){ visit(tree, t); });
}

int main() {
    int n;
    scanf("%d", &n);
    vector<double> ax(n), ay(n);
    for (int i = 0; i < n; ++i) scanf("%lf%lf", &ax[i], &ay[i]);
    struct Edge { int t, c; };
    Graph<Edge> g(n);
    for (int i1 = 0; i1 < n; ++i1)
        for (int i2 = i1 + 1; i2 < n; ++i2) {
            int c = lround(hypot(ax[i1] - ax[i2], ay[i1] - ay[i2]));
            g.v[i1].emplace_back(Edge{i2, c});
            g.v[i2].emplace_back(Edge{i1, c});
        }
    MST<MSTQueueDense<int>, Edge, int> mst(g);
    Graph<int> tree(n);
    for (int i = 1; i < n; ++i)
        tree.v[mst.parent[i]].emplace_back(i);
    visit(tree, 0);
    return 0;
}
