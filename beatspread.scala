// Algorithm: None
object beatspread {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(s, d) = readLine.split(" ").map{_.toInt}
            val a = s - d
            if (a >= 0 && (a & 1) == 0) {
                val b = a >>> 1
                println(s"${s - b} $b")
            } else println("impossible")
        }
    }
}
