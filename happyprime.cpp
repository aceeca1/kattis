// Algorithm: Prime Sieve
#include <cstdio>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

struct Happy {
    int m;
    vector<int> a;

    static int next(int n) {
        auto s = to_string(n);
        int ans = 0;
        for (auto i: s) ans += (i - '0') * (i - '0');
        return ans;
    }

    bool is(int n) {
        if (!a[n]) {
            int k = n;
            vector<int> b;
            while (k > m || !a[k]) {
                if (k <= m) {
                    a[k] = -1;
                    b.emplace_back(k);
                }
                k = next(k);
            }
            int ans = a[k] == -1 ? k : a[k];
            for (auto j: b) a[j] = ans;
        }
        return a[n] == 1;
    }

    Happy(int m_): m(m_), a(m + 1) {
        a[1] = 1;
    }
};

// Snippet: Prime
struct Prime {
    vector<int> a;

    Prime(int n): a(n + 1) {
        int u = 0;
        for (int i = 2; i <= n; ++i) {
            int v = a[i];
            if (!v) u = a[u] = v = i;
            for (int w = 2; i * w <= n; w = a[w]) {
                a[i * w] = w;
                if (w >= v) break;
            }
        }
        a[u] = n + 1;
    }

    bool is(int n) { return n >= 2 && a[n] > n; }
    bool next(int n) { return a[n]; }
};

int main() {
    int n;
    scanf("%d", &n);
    vector<int> no(n), num(n);
    for (int i = 0; i < n; ++i)
        scanf("%d%d", &no[i], &num[i]);
    int m = *max_element(num.begin(), num.end());
    Prime prime(m);
    Happy happy(m);
    for (int i = 0; i < n; ++i)
        if (prime.is(num[i]) && happy.is(num[i]))
            printf("%d %d YES\n", no[i], num[i]);
        else printf("%d %d NO\n", no[i], num[i]);
    return 0;
}
