// Algorithm: None
object sevenwonders {
    import io.StdIn._

    def main(args: Array[String]) {
        var t = 0
        var c = 0
        var g = 0
        for (i <- readLine) i match {
            case 'T' => t += 1
            case 'C' => c += 1
            case 'G' => g += 1
        }
        println((t min c min g) * 7 + t * t + c * c + g * g)
    }
}
