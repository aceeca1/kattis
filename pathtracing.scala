// Algorithm: None
object pathtracing {
    import io.StdIn._

    def readDirections: String = {
        val ret = new StringBuilder
        while (true) {
            val line = readLine
            if (line == null) return ret.toString
            ret.append(line(0))
        }; null
    }

    def forDirections(a: String)(f: (Int, Int) => Unit) = {
        var x, y = 0
        for (i <- a) {
            i match {
                case 'r' => y += 1
                case 'u' => x -= 1
                case 'l' => y -= 1
                case 'd' => x += 1
            }
            f(x, y)
        }
        (x, y)
    }

    def main(args: Array[String]) {
        val a = readDirections
        var xMin, xMax, yMin, yMax = 0
        forDirections(a){(x, y) =>
            if (x < xMin) xMin = x
            if (x > xMax) xMax = x
            if (y < yMin) yMin = y
            if (y > yMax) yMax = y
        }
        val c = Array.fill(xMax - xMin + 1, yMax - yMin + 1){' '}
        val (xEnd, yEnd) = forDirections(a){(x, y) =>
            c(x - xMin)(y - yMin) = '*'
        }
        c(-xMin)(-yMin) = 'S'
        c(xEnd - xMin)(yEnd - yMin) = 'E'
        for (i <- yMin - 1 to yMax + 1) print('#')
        println
        for (i <- xMin to xMax) {
            print('#')
            for (j <- yMin to yMax) print(c(i - xMin)(j - yMin))
            println('#')
        }
        for (i <- yMin - 1 to yMax + 1) print('#')
        println
    }
}
