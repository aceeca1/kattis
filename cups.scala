// Algorithm: None
object cups {
    import io.StdIn._

    case class Cup(radius: Int, name: String)

    def main(args: Array[String]) {
        Array.fill(readInt){
            val line = readLine.split(" ")
            if (line(0)(0).isDigit) Cup(line(0).toInt >>> 1, line(1))
            else Cup(line(1).toInt, line(0))
        }.sortBy(Cup.unapply).foreach{i => println(i.name)}
    }
}
