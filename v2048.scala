// Algorithm: None
object v2048 {
    import io.StdIn._

    class Board(var a: Array[Array[Int]]) {
        override def toString = {
            val sb = new StringBuilder()
            for (i <- a) {
                var head = true
                for (j <- i) {
                    if (!head) sb.append(' ')
                    head = false
                    sb.append(j)
                }
                sb.append('\n')
            }
            sb.toString
        }

        def pressL = for (ai <- a) {
            def f(m: Int, last: Int, z: Int) {() match {
                case _ if m == ai.size =>
                    if (last != -1) {
                        ai(z) = last
                        f(m, -1, z + 1)
                    } else for (i <- z until ai.size) ai(i) = 0
                case _ if ai(m) == 0 => f(m + 1, last, z)
                case _ if last == -1 => f(m + 1, ai(m), z)
                case _ if ai(m) == last =>
                    ai(z) = ai(m) + last
                    f(m + 1, -1, z + 1)
                case _ =>
                    ai(z) = last
                    f(m + 1, ai(m), z + 1)
            }}
            f(0, -1, 0)
        }

        def rotateL {a = Array.tabulate(4, 4){(i, j) => a(j)(3 - i)}}

        def press(n: Int) = {
            for (i <- 0 until n) rotateL
            pressL
            for (i <- n until 4) rotateL
            this
        }
    }

    object Board {
        def read = new Board(Array.fill(4){
            readLine.split(" ").map{_.toInt}
        })
    }

    def main(args: Array[String]) {
        print(Board.read.press(readInt).toString)
    }
}
