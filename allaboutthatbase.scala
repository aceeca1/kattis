// Algorithm: None
object allaboutthatbase {
    import io.StdIn._

    val reEq = "([0-9a-z]+) ([-+*/]) ([0-9a-z]+) = ([0-9a-z]+)".r

    def parseInt(num: String, base: Int) =
        if (base == 1) {
            if (num.exists{_ != '1'}) throw new NumberFormatException
            num.size
        } else Integer.parseInt(num, base)

    def solve(
        x1: String, op: String, x2: String,
        z: String, base: Int
    ): Boolean = {
        try {
            val x1I = parseInt(x1, base)
            val x2I = parseInt(x2, base)
            val zI = parseInt(z, base)
            op match {
                case "+" => x1I + x2I == zI
                case "-" => x1I - x2I == zI
                case "*" => x1I.toLong * x2I == zI
                case "/" => x1I == zI.toLong * x2I
            }
        } catch {
            case e: Exception => false
        }
    }

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val reEq(x1, op, x2, z) = readLine
            val sb = new StringBuilder
            for (base <- 1 to 36)
                if (solve(x1, op, x2, z, base))
                    sb.append(Integer.toString(base, 36).last)
            if (sb.size == 0) println("invalid")
            else println(sb)
        }
    }
}
