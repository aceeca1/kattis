// Algorithm: None
object humancannonball2 {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(v0, a, x, h1, h2) = readLine.split(" ").map{_.toDouble}
            val aR = a * (Math.PI / 180.0)
            val t = x / (v0 * Math.cos(aR))
            val y = v0 * t * Math.sin(aR) - 0.5 * 9.81 * t * t
            if (h1 + 1.0 <= y && y + 1.0 <= h2) println("Safe")
            else println("Not Safe")
        }
    }
}
