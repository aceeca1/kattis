// Algorithm: Sweep Line
object downtime {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, k) = readLine.split(" ").map{_.toInt}
        val a = Array.fill(n){readInt}
        val b = (a.map{i => (i, 1)} ++ a.map{i => (i + 1000, -1)}).sorted
        var m, mc = 0
        for ((_, i) <- b) { mc += i; if (mc > m) m = mc }
        println((m + k - 1) / k)
    }
}
