// Algorithm: None
object different {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            val Array(a1, a2) = line.split(" ").map{_.toLong}
            println(Math.abs(a1 - a2))
        }
    }
}
