// Algorithm: None
object minimumscalar {
    import io.StdIn._

    def main(args: Array[String]) {
        for (caseNo <- 1 to readInt) {
            val n = readInt
            val a1 = readLine.split(" ").map{_.toLong}.sorted
            val a2 = readLine.split(" ").map{_.toLong}.sorted
            val ans = a1.indices.map{i => a1(i) * a2(n - 1 - i)}.sum
            println(s"Case #$caseNo: $ans")
        }
    }
}
