// Algorithm: None
object closingtheloop {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        for (cases <- 1 to readInt) {
            val n = readInt
            var r = Buffer[Int]()
            var b = Buffer[Int]()
            for (i <- readLine.split(" ")) {
                val len = i.init.toInt
                i.last match {
                    case 'R' => r.append(len)
                    case 'B' => b.append(len)
                }
            }
            r = r.sortBy{-_}
            b = b.sortBy{-_}
            val m = r.size min b.size
            val ans = (r.take(m).sum - m) + (b.take(m).sum - m)
            println(s"Case #$cases: $ans")
        }
    }
}
