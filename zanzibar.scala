// Algorithm: None
object zanzibar {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val a = readLine.split(" ").map{_.toInt}
            var ans = 0
            for (i <- 0 to a.size - 2) {
                val d = a(i + 1) - (a(i) << 1)
                if (d >= 0) ans += d
            }
            println(ans)
        }
    }
}
