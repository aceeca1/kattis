// Algorithm: None
object zamka {
    import io.StdIn._

    def sumOfDigits(n: Int) = n.toString.map{_ - '0'}.sum

    def main(args: Array[String]) {
        val l = readInt
        val d = readInt
        val x = readInt
        var n = Int.MaxValue
        var m = Int.MinValue
        for (i <- l to d) if (sumOfDigits(i) == x) {
            if (i < n) n = i
            if (i > m) m = i
        }
        println(n)
        println(m)
    }
}
