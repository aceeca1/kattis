// Algorithm: None
object engineeringenglish {
    import io.StdIn._
    import collection._

    def main(args: Array[String]) {
        val a = new mutable.HashSet[String]
        while (true) {
            val s = readLine
            if (s == null) return
            println(s.split(" ").map{i =>
                val iL = i.toLowerCase
                if (a(iL)) "." else {a.add(iL); i}
            }.mkString(" "))
        }
    }
}
