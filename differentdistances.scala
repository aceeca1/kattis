// Algorithm: None
object differentdistances {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val s = readLine
            if (s == "0") return
            val Array(x1, y1, x2, y2, p) = s.split(" ").map{_.toDouble}
            val ax = Math.pow(Math.abs(x1 - x2), p)
            val ay = Math.pow(Math.abs(y1 - y2), p)
            println(Math.pow(ax + ay, 1.0 / p))
        }
    }
}
