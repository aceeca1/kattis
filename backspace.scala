// Algorithm: None
object backspace {
    import io.StdIn._

    def main(args: Array[String]) {
        val s = readLine
        val sb = new StringBuilder
        for (i <- s) i match {
            case '<' => sb.deleteCharAt(sb.size - 1)
            case _ => sb.append(i)
        }
        println(sb)
    }
}
