// Algorithm: Minimum Spanning Tree
object tsp {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        val n = readInt
        val a = Array.fill(n){readLine.split(" ").map{_.toDouble}}
        val g = Array.fill(n){Buffer[(Int, Int)]()}
        for (i1 <- 0 until n)
            for (i2 <- i1 + 1 until n) {
                val c = Math.hypot(a(i1)(0) - a(i2)(0), a(i1)(1) - a(i2)(1))
                val cR = Math.round(c).toInt
                g(i1).append((i2, cR))
                g(i2).append((i1, cR))
            }
        val mst = new MST[Int] {
            def sizeV = g.size
            def e(no: Int) = g(no).toIterator
            def makeQueue(size: Int) = new MSTQueueDense[Int](size) {
                def infinity = Int.MaxValue
            }
            def infinity = Int.MaxValue
        }
        val tree = Array.fill(n){Buffer[Int]()}
        for (i <- 1 until n) tree(mst.parent(i)).append(i)
        def visit(no: Int) {
            println(no)
            tree(no).foreach(visit)
        }
        visit(0)
    }

    // Snippet: MSTQueueDense
    abstract class MSTQueueDense[T: reflect.ClassTag](n: Int)
        (implicit num: Numeric[T]) extends MSTQueue[T](n) {
        // fake root: -1, no edge: -2, not in queue: -3
        import num._

        def infinity: T

        val parent = Array.fill(n)(-2)
        val cost = Array.fill(n)(infinity)

        def enqueue(s: Int, t: Int, c: T) {
            if (parent(t) == -3) return
            if (c < cost(t)) { cost(t) = c; parent(t) = s }
        }

        def dequeue = {
            val vs = (0 until n).filter{parent(_) != -3}
            if (vs.isEmpty) (-3, -1, zero) else {
                val no = vs.minBy(cost)
                val s = parent(no)
                parent(no) = -3
                (s, no, cost(no))
            }
        }
    }

    // Snippet: MST
    abstract class MSTQueue[T: Numeric](n: Int) {
        def enqueue(s: Int, t: Int, c: T): Unit
        def dequeue: (Int, Int, T)
    }

    abstract class MST[T](implicit num: Numeric[T]) {
        import num._

        def sizeV: Int
        def e(no: Int): Iterator[(Int, T)]
        def makeQueue(size: Int): MSTQueue[T]
        def infinity: T

        val parent = Array.fill(sizeV)(-1)
        var total = zero

        def compute {
            val q = makeQueue(sizeV)
            q.enqueue(-1, 0, zero)
            while (true) {
                val (s, t, c) = q.dequeue
                s match {
                    case -2 => total = infinity; return
                    case -3 => return
                    case _ => ()
                }
                parent(t) = s
                total += c
                for ((et, ec) <- e(t)) q.enqueue(t, et, ec)
            }
        }
        compute
    }
}
