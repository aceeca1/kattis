// Algorithm: Dynamic Programming
object bachetsgame {
    import io.StdIn._
    import collection._

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            val lineS = line.split(" ").map{_.toInt}
            val n = lineS(0)
            val a = lineS.drop(2)
            val win = mutable.BitSet(n + 1)
            for (i <- 0 to n) win(i) = !a.exists{ai => win(i - ai)}
            if (win(n)) println("Ollie wins")
            else println("Stan wins")
        }
    }
}
