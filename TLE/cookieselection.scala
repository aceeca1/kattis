// Algorithm: Segment Tree
object cookieselection {
    import io.StdIn._

    def main(args: Array[String]) {
        val stis = new STIntSet
        while (true) {
            readLine match {
                case null => return
                case "#" =>
                    val a = stis.nth(stis.size >> 1)
                    stis.add(a, -1)
                    println(a)
                case line => stis.add(line.toInt)
            }
        }
    }

    // Snippet: TrieNode
    abstract class TrieNode[T : reflect.ClassTag](n: Int) {
        def newNode: T

        val ch = Array.ofDim[T](n)
        def makeCh(no: Int) = {
            if (ch(no) == null) ch(no) = newNode
            ch(no)
        }
    }

    // Snippet: STIntSet
    class STIntSetNode extends TrieNode[STIntSetNode](2) {
        def newNode = new STIntSetNode
        var size = 0
    }

    class STIntSet {
        var root = new STIntSetNode
        var depth = 0

        def size = root.size

        def add(n: Int, delta: Int = 1) {
            while (n >>> depth != 0) {
                val oldRoot = root
                root = new STIntSetNode
                root.ch(0) = oldRoot
                root.size = oldRoot.size
                depth += 1
            }
            def walk(v: STIntSetNode, depth: Int) {
                v.size += delta
                if (depth == 0) return
                walk(v.makeCh((n >>> depth - 1) & 1), depth - 1)
            }
            walk(root, depth)
        }

        def nth(n: Int) = {
            def walk(v: STIntSetNode, n: Int, depth: Int, ans: Int): Int = {
                if (depth == 0) return ans
                val ch0size = if (v.ch(0) != null) v.ch(0).size else 0
                if (n < ch0size) walk(
                    v.ch(0), n,
                    depth - 1, ans
                ) else walk(
                    v.ch(1), n - ch0size,
                    depth - 1, ans | (1 << depth - 1)
                )
            }
            walk(root, n, depth, 0)
        }
    }
}
