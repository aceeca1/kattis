// Algorithm: Segment Tree
object fenwick {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, q) = readLine.split(" ").map{_.toInt}
        val st = new STSum(n)
        for (_ <- 1 to q) {
            val line = readLine.split(" ")
            line(0) match {
                case "+" => st.fetch(line(1).toInt, line(2).toInt)
                case "?" => println(st.fetch(line(1).toInt, 0))
            }
        }
    }

    // Snippet: STSum
    class STSum(n: Int) {
        val m = 1 << (Math.getExponent(n + 1) + 1)
        val a = Array.ofDim[Int](m + m)

        def fetch(pos: Int, d: Int) = {
            var p = pos + m
            var ans = 0
            while (p != 1) {
                if ((p & 1) != 0) ans += a(p - 1)
                a(p) += d
                p >>>= 1
            }
            ans
        }
    }
}
