// Algorithm: None
object haypoints {
    import io.StdIn._
    import collection._

    class WordV {
        val v = new mutable.HashMap[String, Int].withDefaultValue(0)

        def read(m: Int) {
            for (i <- 1 to m) {
                val line = readLine.split(" ")
                v.update(line(0), line(1).toInt)
            }
        }

        def calcRead: Int = {
            var ans = 0
            while (true) {
                val line = readLine
                if (line == ".") return ans
                for (i <- line.split(" ")) ans += v(i)
            }; ans
        }
    }

    def main(args: Array[String]) {
        val Array(m, n) = readLine.split(" ").map{_.toInt}
        val wordV = new WordV
        wordV.read(m)
        for (i <- 1 to n) println(wordV.calcRead)
    }
}
