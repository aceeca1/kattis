// Algorithm: Union Find
object unionfind {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, q) = readLine.split(" ").map{_.toInt}
        val uf = new UnionFind(n)
        for (_ <- 1 to q) {
            val line = readLine.split(" ")
            val op = line(0)
            val a1 = line(1).toInt
            val a2 = line(2).toInt
            op match {
                case "?" =>
                    if (uf.find(a1) == uf.find(a2)) println("yes")
                    else println("no")
                case "=" => uf.union(a1, a2)
            }
        }
    }

    // Snippet: UnionFind
    class UnionFind(n: Int) {
        val a = Array.range(0, n)
        val z = Array.fill(n){1}

        def find(no: Int) = {
            var root = no
            while (a(root) != root) root = a(root)
            var k = no
            while (a(k) != root) {
                val ak = a(k)
                a(k) = root
                k = ak
            }
            root
        }

        def union(no1: Int, no2: Int) = {
            val root1 = find(no1)
            val root2 = find(no2)
            if (root1 == root2) root1
            else if (z(root1) < z(root2)) {
                a(root1) = root2
                z(root2) += z(root1)
                root2
            } else {
                a(root2) = root1
                z(root1) += z(root2)
                root1
            }
        }
    }
}
