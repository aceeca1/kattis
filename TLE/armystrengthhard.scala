// Algorithm: None
object armystrengthhard {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            readLine
            val Array(ng, nm) = readLine.split(" ").map{_.toInt}
            val ag = readLine.split(" ").map{_.toInt}.max
            val am = readLine.split(" ").map{_.toInt}.max
            if (ag >= am) println("Godzilla")
            else println("MechaGodzilla")
        }
    }
}
