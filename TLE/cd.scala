// Algorithm: None
object cd {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val Array(n, m) = readLine.split(" ").map{_.toInt}
            if (n == 0) return
            val a = Array.fill(n){readInt}
            val b = Array.fill(m){readInt}
            println(a.intersect(b).size)
        }
    }
}
