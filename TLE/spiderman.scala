// Algorithm: Deep First Search
object spiderman {
    import io.StdIn._

    class Spider(a: Array[Int]) {
        val b = Array.ofDim[Int](a.size + 1)
        for (i <- Range(a.size - 1, -1, -1)) b(i) = a(i) + b(i + 1)
        var ans: String = null
        var minHeight = Int.MaxValue
        val sb = new StringBuilder

        def put(height: Int, n: Int) {
            if (sb.size == a.size) {
                if (n == 0) { ans = sb.toString; minHeight = height }
                return
            }
            if (n >= a(sb.size)) {
                sb.append('D')
                put(height, n - a(sb.size - 1))
                sb.deleteCharAt(sb.size - 1)
            }
            val nNew = n + a(sb.size)
            if (nNew < minHeight && nNew <= b(sb.size + 1)) {
                sb.append('U')
                put(height max nNew, nNew)
                sb.deleteCharAt(sb.size - 1)
            }
        }

        def calculate = {
            put(0, 0)
            if (ans == null) "IMPOSSIBLE" else ans
        }
    }

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readInt
            val a = readLine.split(" ").map{_.toInt}
            println(new Spider(a).calculate)
        }
    }
}
