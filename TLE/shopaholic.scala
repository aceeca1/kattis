// Algorithm: None
object shopaholic {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toLong}.sorted
        println(Range(a.size - 3, -1, -3).map(a).sum)
    }
}
