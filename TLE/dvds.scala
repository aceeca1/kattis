// Algorithm: None
object dvds {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readInt
            val a = readLine.split(" ").map{_.toInt}
            val b = Array.ofDim[Int](n)
            for (i <- a.indices) b(a(i) - 1) = i
            var k = 0
            while (k + 1 < b.size && b(k) < b(k + 1)) k += 1
            println(b.size - (k + 1))
        }
    }
}
