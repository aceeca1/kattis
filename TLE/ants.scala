// Algorithm: None
object ants {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(l, n) = readLine.split(" ").map{_.toInt}
            val b = Buffer[Int]()
            while (b.size < n) {
                val a = readLine.split(" ").map{_.toInt}
                b ++= a
            }
            val ans1 = b.map{i => i min l - i}.max
            val ans2 = b.map{i => i max l - i}.max
            println(s"$ans1 $ans2")
        }
    }
}
