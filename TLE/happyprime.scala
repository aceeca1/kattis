// Algorithm: Prime Sieve
object happyprime {
    import io.StdIn._
    import collection.mutable.Buffer

    class Happy(m: Int) {
        val a = Array.ofDim[Int](m + 1)
        a(1) = 1

        def next(k: Int) = k.toString.map{ki =>
            val kiN = ki - '0'
            kiN * kiN
        }.sum

        def is(n: Int) = {
            if (a(n) == 0) {
                var k = n
                val b = Buffer[Int]()
                while (k > m || a(k) == 0) {
                    if (k <= m) {
                        a(k) = -1
                        b.append(k)
                    }
                    k = next(k)
                }
                val ans = if (a(k) == -1) k else a(k)
                for (j <- b) a(j) = ans
            }
            a(n) == 1
        }
    }

    def main(args: Array[String]) {
        val a = Array.fill(readInt){readLine.split(" ").map{_.toInt}}
        val m = a.map{_(1)}.max
        val prime = new Prime(m)
        val happy = new Happy(m)
        for (Array(no, n) <- a) {
            if (prime.is(n) && happy.is(n)) println(s"$no $n YES")
            else println(s"$no $n NO")
        }
    }

    // Snippet: Prime
    class Prime(n: Int) {
        import collection.mutable.Buffer

        val a = Array.ofDim[Int](n + 1)

        {
            var u = 0
            for (i <- 2 to n) {
                var v = a(i)
                if (v == 0) { v = i; a(u) = i; u = i }
                var w = 2
                while (w > 0 && i * w <= n) {
                    a(i * w) = w
                    if (w >= v) w = -1 else w = a(w)
                }
            }
            a(u) = n + 1
        }

        def is(n: Int) = n >= 2 && a(n) > n
        def next(n: Int) = a(n)

        def toBuffer = {
            val ret = Buffer[Int]()
            var i = 2
            while (i <= n) {
                ret.append(i)
                i = a(i)
            }
            ret
        }
    }
}
