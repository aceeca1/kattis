// Algorithm: Greedy
object akcija {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = Array.fill(readInt){readInt}.sortBy{-_}
        println(a.sum - Range(2, a.size, 3).map(a).sum)
    }
}
