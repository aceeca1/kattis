// Algorithm: Prime Sieve
object primesieve {
    import io.StdIn._
    import collection._

    def sieve(
        uZ: Int, u: mutable.BitSet,
        vZ: Int, v: mutable.BitSet,
        start: Int
    )(f: Int => Unit) {
        for (i <- 2 until uZ) if (!u(i)) {
            val lb1 = i * i
            val lb2 = (start + i - 1) / i * i
            val lb = lb1 max lb2
            val ub = start + vZ
            for (j <- Range(lb, ub, i)) v.add(j - start)
        }
        for (i <- 0 until vZ) {
            val num = start + i
            if (num >= 2 && !v(i)) f(num)
        }
    }

    def main(args: Array[String]) {
        val Array(n, q) = readLine.split(" ").map{_.toInt}
        val a = Array.fill(q){readInt}
        val b = new mutable.HashMap[Int, Int]
        for (i <- a.indices) { b(a(i)) = i; a(i) = 0 }
        var ans = 0
        def op(prime: Int) {
            if (prime <= n) ans += 1
            b.get(prime) match {
                case Some(k) => a(k) = 1
                case None => ()
            }
        }
        val m = Math.sqrt(n + 1).toInt + 1
        val u = new mutable.BitSet(m)
        sieve(m, u, m, u, 0)(op)
        for (i <- 1 until m) sieve(m, u, m, new mutable.BitSet(m), i * m)(op)
        println(ans)
        a.foreach(println)
    }
}
