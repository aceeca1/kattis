// Algorithm: Flood Fill
object v10kindsofpeople {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(r, c) = readLine.split(" ").map{_.toInt}
        val a = Array.fill(r){readLine.toCharArray}
        val b = a.map{_.map{_ match {
            case '0' => -1
            case '1' => -2
        }}}
        var no = 0
        for (iX <- 0 until r)
            for (iY <- 0 until c)
                if (b(iX)(iY) < 0) {
                    val color = b(iX)(iY)
                    new FloodFill {
                        def conn = FloodFill.conn4
                        def visit(x: Int, y: Int) { b(x)(y) = no }
                        def sizeX = r
                        def sizeY = c
                        def admissible(x: Int, y: Int) = b(x)(y) == color
                        def expandable(x: Int, y: Int) = true
                    }.fill(iX, iY)
                    no += 1
                }
        for (_ <- 1 to readInt) {
            val line = readLine.split(" ").map{_.toInt}
            val r1 = line(0) - 1
            val c1 = line(1) - 1
            val r2 = line(2) - 1
            val c2 = line(3) - 1
            if (b(r1)(c1) != b(r2)(c2)) println("neither")
            else if (a(r1)(c1) == '0') println("binary")
            else println("decimal")
        }
    }

    // Snippet: FloodFill
    object FloodFill {
        val conn4 = Array((0, 1), (-1, 0), (0, -1), (1, 0))
        val conn8 = Array(
            (0, 1), (-1, 1), (-1, 0), (-1, -1),
            (0, -1), (1, -1), (1, 0), (1, 1)
        )
    }

    abstract class FloodFill {
        import collection._

        def conn: Array[(Int, Int)]
        def visit(x: Int, y: Int): Unit
        def sizeX: Int
        def sizeY: Int
        def admissible(x: Int, y: Int): Boolean
        def expandable(x: Int, y: Int): Boolean

        def fill(x: Int, y: Int) {
            val q = new mutable.Queue[(Int, Int)]
            def meet(x: Int, y: Int) = {
                def xValid = 0 <= x && x < sizeX
                def yValid = 0 <= y && y < sizeY
                if (xValid && yValid && admissible(x, y)) {
                    visit(x, y)
                    if (expandable(x, y)) q.enqueue((x, y))
                }
            }
            meet(x, y)
            while (q.nonEmpty) {
                val (qHX, qHY) = q.dequeue
                for ((dx, dy) <- conn) meet(qHX + dx, qHY + dy)
            }
        }
    }
}
