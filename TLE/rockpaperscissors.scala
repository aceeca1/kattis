// Algorithm: None
object rockpaperscissors {
    import io.StdIn._

    def toNum(s: String) = s match {
        case "rock" => 0
        case "paper" => 1
        case "scissors" => 2
    }

    def main(args: Array[String]) {
        var head = true
        while (true) {
            val line = readLine
            if (line == "0") return
            val Array(n, k) = line.split(" ").map{_.toInt}
            val win = Array.ofDim[Int](n)
            val lose = Array.ofDim[Int](n)
            def winning(p1: Int, p2: Int) {
                win(p1) += 1
                lose(p2) += 1
            }
            for (i <- 1 to k * n * (n - 1) >>> 1) {
                val line = readLine.split(" ")
                val player1 = line(0).toInt - 1
                val player2 = line(2).toInt - 1
                val chose1 = toNum(line(1))
                val chose2 = toNum(line(3))
                (chose2 - chose1 + 3) % 3 match {
                    case 0 => ()
                    case 1 => winning(player2, player1)
                    case 2 => winning(player1, player2)
                }
            }
            if (!head) println
            head = false
            for (i <- 0 until n) {
                val total = win(i) + lose(i)
                if (total == 0) println("-")
                else println("%.3f".format(win(i).toDouble / total))
            }
        }
    }
}
