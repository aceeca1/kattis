// Algorithm: None
object sumkindofproblem {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(k, n) = readLine.split(" ").map{_.toInt}
            val s1 = n * (n + 1) >> 1
            val s2 = n * n
            val s3 = n * (n + 1)
            println(s"$k $s1 $s2 $s3")
        }
    }
}
