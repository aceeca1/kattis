// Algorithm: Binary Search
object low {
    import io.StdIn._

    class Machines(n: Int, k: Int, a: Array[Int]) {
        def isPossible(d: Int): Boolean = {
            var i = 0
            for (u <- 0 until n)
                (i to u * k << 1).find{i1 =>
                    a(i1 + 1) - a(i1) <= d
                } match {
                    case Some(i1) => i = i1 + 2
                    case None => return false
                }
            return true
        }
    }

    def main(args: Array[String]) {
        val Array(n, k) = readLine.split(" ").map{_.toInt}
        val a = readLine.split(" ").map{_.toInt}.sorted
        val mc = new Machines(n, k, a)
        println(bSearch(0, a.max)(mc.isPossible))
    }

    // Snippet: bSearch
    def bSearch(x1: Int, x2: Int)(f: Int => Boolean): Int = {
        if (x1 == x2) return x1
        val m = (x1 + x2) >> 1
        if (f(m)) bSearch(x1, m)(f) else bSearch(m + 1, x2)(f)
    }
}
