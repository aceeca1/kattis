// Algorithm: None
object rationalsequence2 {
    import io.StdIn._

    def f(ans: Int, u: Int, p: Int, q: Int): Int = () match {
        case _ if p == 1 && q == 1 => ans + u
        case _ if p < q => f(ans, u + u, p, q - p)
        case _ if p > q => f(ans + u, u + u, p - q, q)
    }

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val line = readLine.split(" ")
            val k = line(0).toInt
            val Array(p, q) = line(1).split("/").map{_.toInt}
            println("%d %d".format(k, f(0, 1, p, q)))
        }
    }
}
