// Algorithm: None
object raggedright {
    import io.StdIn._
    import collection.mutable.Buffer

    def read: Buffer[Int] = {
        val ret = Buffer[Int]()
        while (true) {
            val s = readLine
            if (s == null) return ret
            ret.append(s.size)
        }; ret
    }

    def main(args: Array[String]) {
        val a = read
        val n = a.max
        a.trimEnd(1)
        println(a.map{m => (n - m) * (n - m)}.sum)
    }
}
