// Algorithm: None
object sibice {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, w, h) = readLine.split(" ").map{_.toInt}
        val m = w * w + h * h
        for (_ <- 0 until n) {
            val k = readInt
            if (k * k <= m) println("DA")
            else println("NE")
        }
    }
}
