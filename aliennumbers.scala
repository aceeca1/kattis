// Algorithm: Converting Base
object aliennumbers {
    import io.StdIn._

    def parse(alien: String, dict: String) = {
        val rDict = Array.ofDim[Int](256)
        for (i <- dict.indices) rDict(dict(i)) = i
        var ans = 0
        for (i <- alien) ans = ans * dict.size + rDict(i)
        ans
    }

    def compose(num: Int, dict: String) = {
        val sb = new StringBuilder
        var n = num
        while (n != 0) {
            sb.append(dict(n % dict.size))
            n /= dict.size
        }
        sb.reverse.toString
    }

    def main(args: Array[String]) {
        for (cases <- 1 to readInt) {
            val Array(alien, src, tgt) = readLine.split(" ")
            val ans = compose(parse(alien, src), tgt)
            println(s"Case #$cases: $ans")
        }
    }
}
