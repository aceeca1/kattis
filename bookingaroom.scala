// Algorithm: None
object bookingaroom {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(r, n) = readLine.split(" ").map{_.toInt}
        val a = Array.ofDim[Boolean](r)
        for (_ <- 1 to n) a(readInt - 1) = true
        a.indexOf(false) match {
            case -1 => println("too late")
            case p => println(p + 1)
        }
    }
}
