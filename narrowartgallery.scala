// Algorithm: Dynamic Programming
object narrowartgallery {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val Array(n, k) = readLine.split(" ").map{_.toInt}
            if (n == 0) return
            val a = Array.fill(n){readLine.split(" ").map{_.toInt}}
            val b = Array.fill(n + 1, k + 1, 3){Int.MaxValue >>> 1}
            b(0)(0)(0) = 0
            def toMin(i: Int, j: Int, m: Int, m1: Int) {
                var t = Int.MaxValue >>> 1
                if (m == 0) t = b(i - 1)(j)(m1)
                else if (j > 0) t = b(i - 1)(j - 1)(m1) + a(i - 1)(m - 1)
                if (t < b(i)(j)(m)) b(i)(j)(m) = t
            }
            for (i <- 1 to n) {
                for (j <- 0 to k) {
                    toMin(i, j, 0, 0); toMin(i, j, 0, 1); toMin(i, j, 0, 2)
                    toMin(i, j, 1, 0); toMin(i, j, 1, 1)
                    toMin(i, j, 2, 0); toMin(i, j, 2, 2)
                }
            }
            println(a.map{_.sum}.sum - b(n)(k).min)
        }
    }
}
