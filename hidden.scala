// Algorithm: None
object hidden {
    import io.StdIn._
    import collection._

    def valid(pass: String, s: String): Boolean = {
        val a = new mutable.HashMap[Char, Int].withDefaultValue(0)
        for (i <- pass) a(i) += 1
        var k = 0
        for (i <- s) if (a(i) != 0) {
            if (i != pass(k)) return false
            a(i) -= 1
            k += 1
        }
        k == pass.size
    }

    def main(args: Array[String]) {
        val Array(pass, s) = readLine.split(" ")
        if (valid(pass, s)) println("PASS")
        else println("FAIL")
    }
}
