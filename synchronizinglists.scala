// Algorithm: None
object synchronizinglists {
    import io.StdIn._

    def main(args: Array[String]) {
        var head = true
        while (true) {
            val n = readInt
            if (n == 0) return
            if (!head) println
            head = false
            val a1 = Array.fill(n){readInt}
            val a2 = Array.fill(n){readInt}.sorted
            val b = Array.range(0, n).sortBy(a1)
            for (i <- 0 until n) a1(b(i)) = a2(i)
            for (i <- 0 until n) println(a1(i))
        }
    }
}
