// Algorithm: None
object spavanac {
    import io.StdIn._

    def main(args: Array[String]) {
        var Array(h, m) = readLine.split(" ").map{_.toInt}
        m -= 45
        if (m < 0) {
            m += 60
            h -= 1
        }
        if (h < 0) h += 24
        println(s"$h $m")
    }
}
