// Algorithm: None
object bus {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            println((1 << readInt) - 1)
        }
    }
}
