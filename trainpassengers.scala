// Algorithm: None
object trainpassengers {
    import io.StdIn._

    def solve: Boolean = {
        val Array(c, n) = readLine.split(" ").map{_.toInt}
        var onTrain = 0
        for (_ <- 1 to n) {
            val Array(left, enter, wait) = readLine.split(" ").map{_.toInt}
            onTrain -= left
            if (onTrain < 0) return false
            onTrain += enter
            if (onTrain > c) return false
            if (onTrain < c && wait > 0) return false
        }
        return onTrain == 0
    }

    def main(args: Array[String]) {
        if (solve) println("possible")
        else println("impossible")
    }
}
