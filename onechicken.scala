// Algorithm: None
object onechicken {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, m) = readLine.split(" ").map{_.toInt}
        def word(a: Int) = if (a == 1) "piece" else "pieces"
        if (n < m) {
            val a = m - n
            println(s"Dr. Chaz will have $a ${word(a)} of chicken left over!")
        } else {
            val a = n - m
            println(s"Dr. Chaz needs $a more ${word(a)} of chicken!")
        }
    }
}
