// Algorithm: Enumerating Divisors
object almostperfect {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            val a = line.toInt
            val b = new Divisor(a).sum - a
            if (a == b) println(s"$a perfect")
            else if (Math.abs(a - b) <= 2) println(s"$a almost perfect")
            else println(s"$a not perfect")
        }
    }

    // Snippet: Divisor
    class Divisor(n: Int) extends Traversable[Int] {
        def foreach[U](f: Int => U) {
            var i = 1
            while (i * i < n) {
                if (n % i == 0) {
                    f(i)
                    f(n / i)
                }
                i += 1
            }
            if (i * i == n) f(i)
        }
    }
}
