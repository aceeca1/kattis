// Algorithm: Flood Fill
object coast {
    import io.StdIn._
    import collection._

    def main(args: Array[String]) {
        val Array(n, m) = readLine.split(" ").map{_.toInt}
        val a = Array.fill(n){readLine.toCharArray}
        val filler = new FloodFill {
            def conn: Array[(Int, Int)] = FloodFill.conn4
            def visit(x: Int, y: Int) {a(x)(y) = 'S'}
            def sizeX = n
            def sizeY = m
            def admissible(x: Int, y: Int) = a(x)(y) == '0'
            def expandable(x: Int, y: Int) = true
        }
        def forEdge(f: (Int, Int) => Unit) {
            for (i <- 0 to n - 1) { f(i, 0); f(i, m - 1) }
            for (i <- 0 to m - 1) { f(0, i); f(n - 1, i) }
        }
        forEdge(filler.fill)
        var coast = 0
        def isCoast(c1: Char, c2: Char) =
            c1 == '1' && c2 == 'S' || c1 == 'S' && c2 == '1'
        for (i <- 0 to n - 2)
            for (j <- 0 to m - 1)
                if (isCoast(a(i)(j), a(i + 1)(j))) coast += 1
        for (i <- 0 to n - 1)
            for (j <- 0 to m - 2)
                if (isCoast(a(i)(j), a(i)(j + 1))) coast += 1
        forEdge{(x, y) => if (a(x)(y) == '1') coast += 1}
        println(coast)
    }

    // Snippet: FloodFill
    object FloodFill {
        val conn4 = Array((0, 1), (-1, 0), (0, -1), (1, 0))
        val conn8 = Array(
            (0, 1), (-1, 1), (-1, 0), (-1, -1),
            (0, -1), (1, -1), (1, 0), (1, 1)
        )
    }

    abstract class FloodFill {
        import collection._

        def conn: Array[(Int, Int)]
        def visit(x: Int, y: Int): Unit
        def sizeX: Int
        def sizeY: Int
        def admissible(x: Int, y: Int): Boolean
        def expandable(x: Int, y: Int): Boolean

        def fill(x: Int, y: Int) {
            val q = new mutable.Queue[(Int, Int)]
            def meet(x: Int, y: Int) = {
                def xValid = 0 <= x && x < sizeX
                def yValid = 0 <= y && y < sizeY
                if (xValid && yValid && admissible(x, y)) {
                    visit(x, y)
                    if (expandable(x, y)) q.enqueue((x, y))
                }
            }
            meet(x, y)
            while (q.nonEmpty) {
                val (qHX, qHY) = q.dequeue
                for ((dx, dy) <- conn) meet(qHX + dx, qHY + dy)
            }
        }
    }
}
