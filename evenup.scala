// Algorithm: Automaton
object evenup {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}
        var ans = 0
        var odd = -1
        for (i <- a) {
            val ai = i & 1
            if (ai == odd) {
                ans -= 1
                odd = if (ans == 0) -1 else 1 - odd
            } else {
                ans += 1
                odd = ai
            }
        }
        println(ans)
    }
}
