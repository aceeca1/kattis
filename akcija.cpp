// Algorithm: Greedy
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int n;
    scanf("%d", &n);
    vector<int> a(n);
    for (int i = 0; i < n; ++i) scanf("%d", &a[i]);
    sort(a.rbegin(), a.rend());
    int ans = 0;
    for (int i = 0; i < n; ++i) ans += a[i];
    for (int i = 2; i < n; i += 3) ans -= a[i];
    printf("%d\n", ans);
    return 0;
}
