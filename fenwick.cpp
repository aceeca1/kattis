// Algorithm: Segment Tree
#include <cstdio>
#include <cmath>
#include <cstdint>
#include <cinttypes>
#include <vector>
using namespace std;

// Snippet: STSum
struct STSum {
    int n, m;
    vector<int64_t> a;

    STSum(int n_): n(n_), m(1 << (ilogb(n + 1) + 1)), a(m + m) {}

    int64_t fetch(int pos, int64_t delta) {
        pos += m;
        int64_t ans = 0;
        for (;; pos >>= 1) {
            a[pos] += delta;
            if (pos == 1) return ans;
            if (pos & 1) ans += a[pos - 1];
        }
    }
};

int main() {
    int n, q;
    scanf("%d%d", &n, &q);
    SegTree st(n);
    while (q--) {
        char c;
        int pos, delta;
        scanf(" %c", &c);
        switch (c) {
            case '+':
                scanf("%d%d", &pos, &delta);
                st.fetch(pos, delta);
                break;
            case '?':
                scanf("%d", &pos);
                printf("%" PRId64 "\n", st.fetch(pos, 0));
        }
    }
    return 0;
}
