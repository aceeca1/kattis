// Algorithm: None
object funhouse {
    import io.StdIn._

    def main(args: Array[String]) {
        var caseNo = 1
        while (true) {
            val Array(w, l) = readLine.split(" ").map{_.toInt}
            if (w == 0) return
            val a = Array.fill(l){readLine.toCharArray}
            val as = a.map{_.indexOf('*')}
            val startX = as.indexWhere{_ != -1}
            val startY = as.find{_ != -1}.get
            val startD = () match {
                case _ if startY == 0 => 0
                case _ if startX == w - 1 => 1
                case _ if startY == l - 1 => 2
                case _ if startX == 0 => 3
            }
            def go(x: Int, y: Int, d: Int): (Int, Int) = {
                val (nX, nY) = d match {
                    case 0 => (x, y + 1)
                    case 1 => (x - 1, y)
                    case 2 => (x, y - 1)
                    case 3 => (x + 1, y)
                }
                a(nX)(nY) match {
                    case 'x' => (nX, nY)
                    case '.' => go(nX, nY, d)
                    case '/' => go(nX, nY, d ^ 1)
                    case '\\' => go(nX, nY, d ^ 3)
                }
            }
            val (endX, endY) = go(startX, startY, startD)
            a(endX)(endY) = '&'
            println(s"HOUSE $caseNo")
            a.foreach{ai => println(new String(ai))}
            caseNo += 1
        }
    }
}
