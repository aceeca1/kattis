// Algorithm: None
object busnumbers {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}.sorted
        println(new PartitionBy[Int, String] {
            def in = a
            def eqv(a1: Int, a2: Int) = a2 - a1 == 1
            def mapTo = new Aggregator[Int, String] {
                var aH, aT = -1
                def add(data: Int) {
                    if (aH == -1) aH = data
                    aT = data
                }
                def result = aT - aH match {
                    case 0 => s"$aH"
                    case 1 => s"$aH $aT"
                    case _ => s"$aH-$aT"
                }
            }
        }.mkString(" "))
    }

    // Snippet: PartitionBy
    abstract class Aggregator[T, U] {
        def add(data: T): Unit
        def result: U
    }

    abstract class PartitionBy[T, U] extends Traversable[U] {
        def in: Traversable[T]
        def eqv(a1: T, a2: T): Boolean
        def mapTo: Aggregator[T, U]

        def foreach[V](f: U => V) {
            var data = null.asInstanceOf[T]
            var aggr: Aggregator[T, U] = null
            for (i <- in)
                if (aggr == null) {
                    data = i
                    aggr = mapTo
                    aggr.add(i)
                } else if (eqv(data, i)) {
                    data = i
                    aggr.add(i)
                } else {
                    f(aggr.result)
                    data = i
                    aggr = mapTo
                    aggr.add(i)
                }
            if (aggr != null) f(aggr.result)
        }
    }
}
