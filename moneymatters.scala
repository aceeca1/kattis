// Algorithm: Deep First Search
object moneymatters {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        val Array(n, m) = readLine.split(" ").map{_.toInt}
        val a = Array.fill(n){readInt}
        val g = Array.fill(n){Buffer[Int]()}
        for (_ <- 1 to m) {
            val Array(v1, v2) = readLine.split(" ").map{_.toInt}
            g(v1).append(v2)
            g(v2).append(v1)
        }
        val cc = new ConnectedComponent {
            def sizeV = g.size
            def e(no: Int) = g(no).toIterator
        }
        val b = Array.ofDim[Int](cc.num)
        for (i <- 0 until n) b(cc.label(i)) += a(i)
        if (b.forall{_ == 0}) println("POSSIBLE")
        else println("IMPOSSIBLE")
    }

    // Snippet: ConnectedComponent
    abstract class ConnectedComponent {
        import collection._
        import collection.mutable.Buffer

        def sizeV: Int
        def e(no: Int): Iterator[Int]

        val label = Array.fill(sizeV){-1}
        var num = 0

        def visit(no: Int) {
            label(no) = num
            for (t <- e(no)) if (label(t) == -1) visit(t)
        }

        for (i <- 0 until sizeV)
            if (label(i) == -1) { visit(i); num += 1 }
    }
}
