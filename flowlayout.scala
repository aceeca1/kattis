// Algorithm: None
object flowlayout {
    import io.StdIn._

    def readSolve(m: Int): String = {
        var totalW = 0
        var totalH = 0
        var currentW = 0
        var currentH = 0
        def currentFinish = {
            if (currentW > totalW) totalW = currentW
            totalH += currentH
            currentW = 0
            currentH = 0
        }
        while (true) {
            val Array(w, h) = readLine.split(" ").map{_.toInt}
            if (w == -1) {
                currentFinish
                return s"$totalW x $totalH"
            }
            if (currentW + w > m) currentFinish
            currentW += w
            if (h > currentH) currentH = h
        }; null
    }

    def main(args: Array[String]) {
        while (true) {
            val m = readInt
            if (m == 0) return
            println(readSolve(m))
        }
    }
}
