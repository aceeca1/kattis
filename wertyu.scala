// Algorithm: None
object wertyu {
    import io.StdIn._

    val keyb = "`1234567890-=QWERTYUIOP[]\\ASDFGHJKL;'ZXCVBNM,./"
    val keybPrev = Array.ofDim[Char](256)
    keybPrev(' ') = ' '
    for (i <- 1 until keyb.size) keybPrev(keyb(i)) = keyb(i - 1)

    def main(args: Array[String]) {
        while (true) {
            val s = readLine
            if (s == null) return
            println(s.map{keybPrev(_)})
        }
    }
}
