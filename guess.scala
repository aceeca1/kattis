// Algorithm: Binary Search
object guess {
    import io.StdIn._

    def main(args: Array[String]) {
        var s = 1
        var t = 1000
        while (true) {
            val mid = (s + t) >> 1
            println(mid)
            readLine match {
                case "lower" => t = mid - 1
                case "higher" => s = mid + 1
                case "correct" => return
            }
        }
    }
}
