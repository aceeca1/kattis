// Algorithm: None
object guessthedatastructure {
    import io.StdIn._
    import collection._

    abstract class Bag(val name: String) {
        def into(n: Int)
        def from: Int
    }

    class FIFOBag(name: String) extends Bag(name) {
        val a = new mutable.Queue[Int]
        override def into(n: Int) { a.enqueue(n) }
        override def from = if (a.nonEmpty) a.dequeue else -1
    }

    class LIFOBag(name: String) extends Bag(name) {
        val a = new mutable.ArrayStack[Int]
        override def into(n: Int) { a.push(n) }
        override def from = if (a.nonEmpty) a.pop else -1
    }

    class PrioBag(name: String) extends Bag(name) {
        val a = new mutable.PriorityQueue[Int]
        override def into(n: Int) { a.enqueue(n) }
        override def from = if (a.nonEmpty) a.dequeue else -1
    }

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            val n = line.toInt
            val bags = Array(
                new FIFOBag("queue"),
                new LIFOBag("stack"),
                new PrioBag("priority queue"))
            val valid = Array(true, true, true)
            for (_ <- 1 to n) {
                val Array(comm, elem) = readLine.split(" ").map{_.toInt}
                comm match {
                    case 1 =>
                        for (i <- bags) i.into(elem)
                    case 2 =>
                        for (i <- 0 to 2)
                            if (bags(i).from != elem) valid(i) = false
                }
            }
            println(valid.count{i => i} match {
                case 0 => "impossible"
                case 1 => bags(valid.indexOf(true)).name
                case _ => "not sure"
            })
        }
    }
}
