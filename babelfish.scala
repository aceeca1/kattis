// Algorithm: None
object babelfish {
    import io.StdIn._
    import collection._

    def readDict: mutable.HashMap[String, String] = {
        val ret = new mutable.HashMap[String, String]
        while (true) {
            val s = readLine
            if (s.size == 0) return ret
            val Array(en, fr) = s.split(" ")
            ret(fr) = en
        }; null
    }

    def main(args: Array[String]) {
        val dict = readDict
        while (true) {
            val s = readLine
            if (s == null) return
            println(dict.getOrElse(s, "eh"))
        }
    }
}
