// Algorithm: None
object permutedarithmeticsequence {
    import io.StdIn._

    def isArith(a: Array[Int]): Boolean = {
        val d = a(1) - a(0)
        (0 to a.size - 2).forall{i => a(i + 1) - a(i) == d}
    }

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val a = readLine.split(" ").map{_.toInt}.drop(1)
            if (isArith(a)) println("arithmetic")
            else if (isArith(a.sorted)) println("permuted arithmetic")
            else println("non-arithmetic")
        }
    }
}
