// Algorithm: None
object apaxiaaans {
    import io.StdIn._

    def main(args: Array[String]) {
        val s = readLine
        println(new PartitionBy[Char, Char] {
            def in = s
            def eqv(a1: Char, a2: Char) = a1 == a2
            def mapTo = new Aggregator[Char, Char] {
                var result = null.asInstanceOf[Char]
                def add(data: Char) { result = data }
            }
        }.mkString)
    }

    // Snippet: PartitionBy
    abstract class Aggregator[T, U] {
        def add(data: T): Unit
        def result: U
    }

    abstract class PartitionBy[T, U] extends Traversable[U] {
        def in: Traversable[T]
        def eqv(a1: T, a2: T): Boolean
        def mapTo: Aggregator[T, U]

        def foreach[V](f: U => V) {
            var data = null.asInstanceOf[T]
            var aggr: Aggregator[T, U] = null
            for (i <- in)
                if (aggr == null) {
                    data = i
                    aggr = mapTo
                    aggr.add(i)
                } else if (eqv(data, i)) {
                    data = i
                    aggr.add(i)
                } else {
                    f(aggr.result)
                    data = i
                    aggr = mapTo
                    aggr.add(i)
                }
            if (aggr != null) f(aggr.result)
        }
    }
}
