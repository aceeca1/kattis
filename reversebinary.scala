// Algorithm: None
object reversebinary {
    import io.StdIn._

    def rev(n: Int, ans: Int = 0): Int = {
        if (n == 0) return ans
        if ((n & 1) != 0) rev(n >>> 1, (ans << 1) + 1)
        else rev(n >>> 1, ans << 1)
    }

    def main(args: Array[String]) {
        println(rev(readInt))
    }
}
