// Algorithm: Greedy
object safepassage {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = readLine.split(" ").map{_.toInt}.tail.sorted
        val k = a(0) + a(1) + a(1)
        def f(ans: Int, i: Int): Int =
            if (a(i - 1) + a(0) + a(0) > k) f(ans + a(i) + k, i - 2)
            else ans + Range(1, i + 1).map{u => a(u) + a(0)}.sum - a(0)
        println(f(0, a.size - 1))
    }
}
