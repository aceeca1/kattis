// Algorithm: None
object whatdoesthefoxsay {
    import io.StdIn._
    import collection._

    def readSays: mutable.HashSet[String] = {
        val a = mutable.HashSet[String]()
        while (true) {
            val s = readLine
            if (s == "what does the fox say?") return a
            a.add(s.split(" goes ")(1))
        }; a
    }

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val s = readLine.split(" ")
            val a = readSays
            println(s.filter{i => !a(i)}.mkString(" "))
        }
    }
}
