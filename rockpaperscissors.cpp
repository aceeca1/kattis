// Algorithm: None
#include <cstdio>
#include <string>
#include <vector>
using namespace std;

int toNum(const string& s) {
    if (s == "rock") return 0;
    if (s == "paper") return 1;
    return 2;
}

struct WinLose {
    vector<int> win, lose;
    WinLose(int n): win(n), lose(n) {}

    void winning(int p1, int p2) {
        ++win[p1];
        ++lose[p2];
    }
};

int main() {
    bool head = true;
    for (;;) {
        int n, k;
        scanf("%d", &n);
        if (!n) break;
        scanf("%d", &k);
        WinLose wl(n);
        for (int i = k * n * (n - 1) >> 1; i; --i) {
            int player1, player2;
            char chose1[11], chose2[11];
            scanf("%d%s%d%s", &player1, chose1, &player2, chose2);
            --player1;
            --player2;
            int chose1n = toNum(chose1);
            int chose2n = toNum(chose2);
            switch ((chose2n - chose1n + 3) % 3) {
                case 1: wl.winning(player2, player1); break;
                case 2: wl.winning(player1, player2); break;
            }
        }
        if (!head) printf("\n");
        head = false;
        for (int i = 0; i < n; ++i) {
            int total = wl.win[i] + wl.lose[i];
            if (!total) printf("-\n");
            else printf("%.3f\n", double(wl.win[i]) / total);
        }
    }
    return 0;
}
