// Algorithm: None
object semafori {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, l) = readLine.split(" ").map{_.toInt}
        var timePassed = 0
        var currentPosition = 0
        for (_ <- 0 until n) {
            val Array(d, r, g) = readLine.split(" ").map{_.toInt}
            timePassed += d - currentPosition
            currentPosition = d
            val timeInCycle = timePassed % (r + g)
            timePassed += (timeInCycle max r) - timeInCycle
        }
        timePassed += l - currentPosition
        currentPosition = l
        println(timePassed)
    }
}
