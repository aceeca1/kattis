// Algorithm: None
object dicegame {
    import io.StdIn._

    def main(args: Array[String]) {
        val a1 = readLine.split(" ").map{_.toInt}.sum
        val a2 = readLine.split(" ").map{_.toInt}.sum
        println(
            if (a1 < a2) "Emma"
            else if (a1 == a2) "Tie"
            else "Gunnar")
    }
}
