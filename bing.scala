// Algorithm: Trie
object bing {
    import io.StdIn._

    class TNode extends TrieNode[TNode](26) {
        def newNode = new TNode
        var size = 0
    }

    class Trie {
        val root = new TNode

        final def fetch(s: String, v: TNode = root, depth: Int = 0): Int = {
            v.size += 1
            if (s.size == depth) return v.size - 1
            fetch(s, v.makeCh(s(depth) - 'a'), depth + 1)
        }
    }

    def main(args: Array[String]) {
        val trie = new Trie
        for (_ <- 1 to readInt) println(trie.fetch(readLine))
    }

    // Snippet: TrieNode
    abstract class TrieNode[T : reflect.ClassTag](n: Int) {
        def newNode: T

        val ch = Array.ofDim[T](n)
        def makeCh(no: Int) = {
            if (ch(no) == null) ch(no) = newNode
            ch(no)
        }
    }
}
