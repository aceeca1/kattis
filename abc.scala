// Algorithm: None
object abc {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = readLine.split(" ").map{_.toInt}.sorted
        println(readLine.map{i => a(i - 'A')}.mkString(" "))
    }
}
