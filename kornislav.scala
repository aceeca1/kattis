// Algorithm: None
object kornislav {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = readLine.split(" ").map{_.toInt}.sorted
        println(a(0) * a(2))
    }
}
