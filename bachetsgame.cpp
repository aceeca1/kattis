// Algorithm: Dynamic Programming
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    for (;;) {
        int n, m;
        if (scanf("%d", &n) != 1) break;
        scanf("%d", &m);
        vector<int> a(m);
        for (int i = 0; i < m; ++i) scanf("%d", &a[i]);
        vector<bool> win(n + 1);
        for (int i = 0; i <= n; ++i)
            win[i] = none_of(a.begin(), a.end(), [&](int ai) {
                return i - ai >= 0 && win[i - ai];
            });
        if (win[n]) printf("Ollie wins\n");
        else printf("Stan wins\n");
    }
    return 0;
}
