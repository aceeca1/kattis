// Algorithm: None
object everywhere {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt)
            println(Iterator.fill(readInt)(readLine).toSet.size)
    }
}
