// Algorithm: None
object justaminute {
    import io.StdIn._

    def main(args: Array[String]) {
        var m = 0.0
        var s = 0.0
        for (_ <- 1 to readInt) {
            val Array(mi, si) = readLine.split(" ").map{_.toDouble}
            m += mi
            s += si
        }
        val ans = s / (m * 60.0)
        if (ans <= 1.0) println("measurement error")
        else println(ans)
    }
}
