// Algorithm: None
object ones {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            val n = line.toInt
            var ans = 1
            var a = 1 % n
            while (a != 0) {
                a = (a * 10 + 1) % n
                ans += 1
            }
            println(ans)
        }
    }
}
