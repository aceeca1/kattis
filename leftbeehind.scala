// Algorithm: None
object leftbeehind {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val Array(sweet, sour) = readLine.split(" ").map{_.toInt}
            if (sweet == 0 && sour == 0) return
            if (sweet + sour == 13) println("Never speak again.")
            else if (sweet < sour) println("Left beehind.")
            else if (sweet == sour) println("Undecided.")
            else println("To the convention.")
        }
    }
}
