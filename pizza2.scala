// Algorithm: None
object pizza2 {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(r, c) = readLine.split(" ").map{_.toDouble}
        def pow2(x: Double) = x * x
        println(pow2((r - c) / r) * 100.0)
    }
}
