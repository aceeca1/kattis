// Algorithm: None
object numbertree {
    import io.StdIn._

    def main(args: Array[String]) {
        val line = readLine.split(" ", -1)
        val n = line(0).toInt
        val s = line(1)
        var m = 1
        for (i <- s) i match {
            case 'L' => m = m + m
            case 'R' => m = m + m + 1
        }
        println((1 << (n + 1)) - m)
    }
}
