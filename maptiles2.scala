// Algorithm: None
object maptiles2 {
    import io.StdIn._

    def main(args: Array[String]) {
        var x = 0
        var y = 0
        val line = readLine
        for (i <- line) i match {
            case '0' => x += x;      y += y
            case '1' => x += x + 1;  y += y
            case '2' => x += x;      y += y + 1
            case '3' => x += x + 1;  y += y + 1
        }
        println("%d %d %d".format(line.size, x, y))
    }
}
