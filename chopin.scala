// Algorithm: None
object chopin {
    import io.StdIn._
    import collection._

    val a1 = Array("A#", "C#", "D#", "F#", "G#")
    val a2 = Array("Bb", "Db", "Eb", "Gb", "Ab")
    val a = new mutable.HashMap[String, String]
    for (i <- a1.indices) {
        a.update(a1(i), a2(i))
        a.update(a2(i), a1(i))
    }

    def main(args: Array[String]) {
        var caseNo = 1
        while (true) {
            val line = readLine
            if (line == null) return
            val Array(note, tonality) = line.split(" ")
            a.get(note) match {
                case Some(k) => println(s"Case $caseNo: $k $tonality")
                case None => println(s"Case $caseNo: UNIQUE")
            }
            caseNo += 1
        }
    }
}
