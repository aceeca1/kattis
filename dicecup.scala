// Algorithm: None
object dicecup {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, m) = readLine.split(" ").map{_.toInt}.sorted
        for (i <- n + 1 to m + 1) println(i)
    }
}
