// Algorithm: None
object compass {
    import io.StdIn._

    def main(args: Array[String]) {
        val n1 = readInt
        val n2 = readInt
        println((n2 - n1 + (360 + 179)) % 360 - 179)
    }
}
