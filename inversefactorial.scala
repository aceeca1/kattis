// Algorithm: None
object inversefactorial {
    import io.StdIn._

    def invFac(a: Double): Int = {
        var aVar = a
        var ans = 1
        while (true) {
            val k = Math.log(ans)
            if (aVar < k * 0.5) return ans - 1
            aVar -= k
            ans += 1
        }; 0
    }

    def main(args: Array[String]) {
        val s = readLine
        val a = Math.log(("0." + s).toDouble) + Math.log(10.0) * s.size
        println(invFac(a))
    }
}
