// Algorithm: None
object pivot {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}
        val a1 = Array.ofDim[Int](a.size); {
            var k = Int.MinValue
            for (i <- a.indices) {
                a1(i) = k
                if (a(i) > k) k = a(i)
            }
        }
        val a2 = Array.ofDim[Int](a.size); {
            var k = Int.MaxValue
            for (i <- Range(a.size - 1, -1, -1)) {
                a2(i) = k
                if (a(i) < k) k = a(i)
            }
        }
        println(a.indices.count{i => a1(i) <= a(i) && a(i) <= a2(i)})
    }
}
