// Algorithm: None
object statistics {
    import io.StdIn._

    def main(args: Array[String]) {
        var cases = 1
        while (true) {
            val line = readLine
            if (line == null) return
            val a = line.split(" ").map{_.toInt}.drop(1)
            val max = a.max
            val min = a.min
            val range = max - min
            println(s"Case $cases: $min $max $range")
            cases += 1
        }
    }
}
