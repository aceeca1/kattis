// Algorithm: None
#include <cstdio>
#include <cstring>
#include <string>
#include <unordered_map>
using namespace std;

struct WordV {
    unordered_map<string, int> v;

    void read(int m) {
        for (int i = 0; i < m; ++i) {
            char word[17];
            int salary;
            scanf("%s%d", word, &salary);
            v[word] = salary;
        }
    }

    int calcRead() {
        int ans = 0;
        while (true) {
            char word[201];
            scanf("%s", word);
            if (!strcmp(word, ".")) break;
            ans += v[word];
        }
        return ans;
    }
};

int main() {
    int m, n;
    scanf("%d%d", &m, &n);
    WordV wordV;
    wordV.read(m);
    for (int i = 0; i < n; ++i)
        printf("%d\n", wordV.calcRead());
    return 0;
}
