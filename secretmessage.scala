// Algorithm: None
object secretmessage {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val s = readLine
            var n = 1
            while (n * n < s.size) n += 1
            val a = Array.ofDim[Char](n, n)
            var k = 0
            for (i <- Range(n - 1, -1, -1))
                for (j <- 0 until n) {
                    a(j)(i) = if (k < s.size) s(k) else '*'
                    k += 1
                }
            val sb = new StringBuilder()
            for (i <- 0 until n)
                for (j <- 0 until n) {
                    val v = a(i)(j)
                    if (v != '*') sb.append(v)
                }
            println(sb)
        }
    }
}
