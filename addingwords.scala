// Algorithm: None
object addingwords {
    import io.StdIn._
    import collection._

    class Memory {
        val a1 = new mutable.HashMap[String, Int]
        val a2 = new mutable.HashMap[Int, String]

        def iDef(word: String, num: Int) {
            a1.get(word) match {
                case Some(n) => a2.remove(n)
                case None => ()
            }
            a1.update(word, num)
            a2.update(num, word)
        }

        def iCalc(expr: Array[String]) = {
            expr.mkString(" ") + " = " + (try {
                var ans = a1(expr(0))
                for (i <- Range(1, expr.size, 2)) expr(i) match {
                    case "+" => ans += a1(expr(i + 1))
                    case "-" => ans -= a1(expr(i + 1))
                }
                a2(ans)
            } catch {
                case e: NoSuchElementException => "unknown"
            })
        }

        def iClear {
            a1.clear
            a2.clear
        }
    }

    def main(args: Array[String]) {
        val mem = new Memory
        while (true) {
            val line = readLine
            if (line == null) return
            val lineS = line.split(" ")
            lineS(0) match {
                case "def" => mem.iDef(lineS(1), lineS(2).toInt)
                case "calc" =>
                    println(mem.iCalc(lineS.slice(1, lineS.size - 1)))
                case "clear" => mem.iClear
            }
        }
    }
}
