// Algorithm: None
object easiest {
    import io.StdIn._

    def sumOfDigits(n: Int) = n.toString.map{_ - '0'}.sum

    def main(args: Array[String]) {
        while (true) {
            val n = readInt
            if (n == 0) return
            val s = sumOfDigits(n)
            println(Iterator.from(11).find{p => sumOfDigits(p * n) == s}.get)
        }
    }
}
