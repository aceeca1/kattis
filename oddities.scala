// Algorithm: None
object oddities {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readInt
            val oe = if ((n & 1) != 0) "odd" else "even"
            println(s"$n is $oe")
        }
    }
}
