// Algorithm: None
object anotherbrick {
    import io.StdIn._

    def complete(h: Int, w: Int, a: Array[Int]): Boolean = {
        var currentH = 0
        var currentW = 0
        for (i <- a) {
            val left = w - currentW
            if (i < left) currentW += i
            else if (i == left) {
                currentW = 0
                currentH += 1
                if (currentH == h) return true
            } else return false
        }; false
    }

    def main(args: Array[String]) {
        val Array(h, w, n) = readLine.split(" ").map{_.toInt}
        val a = readLine.split(" ").map{_.toInt}
        if (complete(h, w, a)) println("YES")
        else println("NO")
    }
}
