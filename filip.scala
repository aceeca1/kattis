// Algorithm: None
object filip {
    import io.StdIn._

    def main(args: Array[String]) {
        println(readLine.split(" ").map{_.reverse.toInt}.max)
    }
}
