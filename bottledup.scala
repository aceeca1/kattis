// Algorithm: None
object bottledup {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(s, v1, v2) = readLine.split(" ").map{_.toInt}
        def f(k2: Int): Option[(Int, Int)] = {
            val a2 = v2 * k2
            val a1 = s - a2
            if (a1 < 0) return None
            if (a1 % v1 == 0) return Some((a1 / v1, k2))
            f(k2 + 1)
        }
        f(0) match {
            case Some((k1, k2)) => println(s"$k1 $k2")
            case None => println("Impossible")
        }
    }
}
