// Algorithm: None
object billiard {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val Array(a, b, s, m, n) = readLine.split(" ").map{_.toInt}
            if (a == 0) return
            val x = a * m
            val y = b * n
            val z = Math.hypot(x, y)
            val angle = Math.atan2(y, x) * (180.0 / Math.PI)
            println("%.2f %.2f".format(angle, z / s))
        }
    }
}
