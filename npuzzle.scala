// Algorithm: None
object npuzzle {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = Array.fill(4){readLine}
        var ans = 0
        for (i1 <- 0 until 4)
            for (i2 <- 0 until 4) if (a(i1)(i2) != '.') {
                val ai = a(i1)(i2) - 'A'
                val ai1 = ai >> 2
                val ai2 = ai & 3
                ans += Math.abs(ai1 - i1) + Math.abs(ai2 - i2)
            }
        println(ans)
    }
}
