// Algorithm: None
object toilet {
    import io.StdIn._

    def solve(s: String)(f: Char => Char) = {
        var state = s(0)
        var ans = 0
        for (i <- 1 until s.size) {
            if (state != s(i)) {
                state = s(i)
                ans += 1
            }
            val finish = f(s(i))
            if (state != finish) {
                state = finish
                ans += 1
            }
        }
        ans
    }

    def main(args: Array[String]) {
        val s = readLine
        println(solve(s){i => 'U'})
        println(solve(s){i => 'D'})
        println(solve(s){i => i})
    }
}
