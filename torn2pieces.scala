// Algorithm: Shortest Path
object torn2pieces {
    import io.StdIn._

    def main(args: Array[String]) {
        val g = new GraphV[String, Int]
        for (i <- 1 to readInt) {
            val line = readLine.split(" ")
            val no = g.addVertex(line(0))
            for (i <- 1 until line.size) {
                val no1 = g.addVertex(line(i))
                g.e(no).append(no1)
                g.e(no1).append(no)
            }
        }
        val name = Array.ofDim[String](g.v.size)
        for ((s, i) <- g.v) name(i) = s
        val line = readLine.split(" ")
        val s = g.addVertex(line(0))
        val t = g.addVertex(line(1))
        val sp = new ShortPathU {
            def sizeV = g.e.size
            def e(no: Int) = g.e(no).toIterator
            def src = s
        }
        if (sp.dis(t) == Int.MaxValue) println("no route found")
        else println(sp.route(t).map(name).mkString(" "))
    }

    // Snippet: GraphV
    class GraphV[V, E] {
        import collection._
        import collection.mutable.Buffer

        val v = new mutable.HashMap[V, Int]
        val e = Buffer[Buffer[E]]()

        def addVertex = {
            e.append(Buffer[E]())
            e.size - 1
        }

        def addVertex(label: V): Int = v.get(label) match {
            case Some(n) => n
            case None => v.update(label, e.size); addVertex
        }
    }

    // Snippet: ShortPathU
    abstract class ShortPathU {
        import collection._
        import collection.mutable.Buffer

        def sizeV: Int
        def e(no: Int): Iterator[Int]
        def src: Int

        val dis = Array.fill(sizeV){Int.MaxValue}
        val from = Array.ofDim[Int](sizeV)

        {
            val q = new mutable.Queue[Int]
            q.enqueue(src)
            dis(src) = 0
            while (q.nonEmpty) {
                val qH = q.dequeue
                for (t <- e(qH)) {
                    if (dis(t) == Int.MaxValue) {
                        dis(t) = dis(qH) + 1
                        from(t) = qH
                        q.enqueue(t)
                    }
                }
            }
        }

        def route(dst: Int) = {
            val ans = Buffer[Int]()
            var no = dst
            while (no != src) {
                ans.append(no)
                no = from(no)
            }
            ans.append(src)
            ans.reverse
        }
    }
}
