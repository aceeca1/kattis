// Algorithm: None
object twostones {
    import io.StdIn._

    def main(args: Array[String]) {
        val winner = if ((readInt & 1) != 0) "Alice" else "Bob"
        println(winner)
    }
}
