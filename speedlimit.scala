// Algorithm: None
object speedlimit {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val n = readInt
            if (n == -1) return
            var time0 = 0
            var total = 0
            for (i <- 1 to n) {
                val Array(s, t) = readLine.split(" ").map{_.toInt}
                total += s * (t - time0)
                time0 = t
            }
            println(s"$total miles")
        }
    }
}
