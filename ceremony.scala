// Algorithm: None
object ceremony {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}.sorted
        println(a.indices.map{i => n - 1 - i + a(i)}.min min n)
    }
}
