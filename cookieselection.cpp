// Algorithm: Segment Tree
#include <cstdio>
#include <vector>
#include <memory>
using namespace std;

// Snippet: TrieNode
template <class T>
struct TrieNode {
    vector<unique_ptr<T>> ch;
    TrieNode(int n): ch(n) {}

    T* makeCh(int no) {
        if (!ch[no]) ch[no] = unique_ptr<T>(new T(ch.size()));
        return ch[no].get();
    }
};

// Snippet: STIntSet
struct STIntSetNode: TrieNode<STIntSetNode> {
    int size = 0;
    STIntSetNode(int n = 26): TrieNode(n) {}
};

struct STIntSet {
    using uNode = unique_ptr<STIntSetNode>;
    uNode root = uNode(new STIntSetNode);
    int depth = 0;

    int size() { return root->size; }

    void add(int n, int delta = 1) {
        while (n >> depth) {
            auto oldRoot = move(root);
            root = uNode(new STIntSetNode);
            root->ch[0] = move(oldRoot);
            root->size = root->ch[0]->size;
            ++depth;
        }
        auto current = root.get();
        for (int i = depth;; --i) {
            current->size += delta;
            if (!i) return;
            current = current->makeCh((n >> (i - 1)) & 1);
        }
    }

    int nth(int n) const {
        int ans = 0;
        auto current = root.get();
        for (int i = depth;; --i) {
            if (!i) return ans;
            int ch0size = current->ch[0] ? current->ch[0]->size : 0;
            if (n >= ch0size) {
                n -= ch0size;
                ans |= 1 << i - 1;
                current = current->ch[1].get();
            } else current = current->ch[0].get();
        }
    }
};


int main() {
    STIntSet stis;
    for (;;) {
        int n;
        char c;
        if (scanf("%d", &n) == 1) stis.add(n);
        else if (scanf(" %c", &c) == 1 && c == '#') {
            int a = stis.nth(stis.size() >> 1);
            stis.add(a, -1);
            printf("%d\n", a);
        } else break;
    }
    return 0;
}

