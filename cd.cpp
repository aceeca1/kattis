// Algorithm: None
#include <cstdio>
#include <unordered_set>
using namespace std;

int main() {
    for (;;) {
        int n, m, ans = 0;
        scanf("%d%d", &n, &m);
        if (!n) break;
        unordered_set<int> a;
        for (int i = 0; i < n; ++i) {
            int k;
            scanf("%d", &k);
            a.emplace(k);
        }
        for (int i = 0; i < m; ++i) {
            int k;
            scanf("%d", &k);
            if (a.count(k)) ++ans;
        }
        printf("%d\n", ans);
    }
    return 0;
}
