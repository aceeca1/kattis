// Algorithm: None
object carrots {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = readLine.split(" ").map{_.toInt}
        println(a(1))
    }
}
