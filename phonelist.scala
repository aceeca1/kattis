// Algorithm: None
object phonelist {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val a = Array.fill(readInt){readLine}.sorted
            val ans = (0 to a.size - 2).exists{i => a(i + 1).startsWith(a(i))}
            println(if (ans) "NO" else "YES")
        }
    }
}
