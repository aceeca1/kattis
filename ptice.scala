// Algorithm: None
object ptice {
    import io.StdIn._

    def cyc(s: String) = Iterator.continually(s).flatMap{i => i}

    def diff(s1: String, s2: Iterator[Char]) = {
        var ans = 0
        for (i <- s1) if (i == s2.next) ans += 1
        ans
    }

    def main(args: Array[String]) {
        val n = readInt
        val s = readLine
        val adrian = diff(s, cyc("ABC"))
        val bruno = diff(s, cyc("BABC"))
        val goran = diff(s, cyc("CCAABB"))
        val ans = adrian max bruno max goran
        println(ans)
        if (adrian == ans) println("Adrian")
        if (bruno == ans) println("Bruno")
        if (goran == ans) println("Goran")
    }
}
