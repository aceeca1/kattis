// Algorithm: Flood Fill
object countingstars {
    import io.StdIn._

    def main(args: Array[String]) {
        var cases = 1
        while (true) {
            val line = readLine
            if (line == null) return
            val Array(m, n) = line.split(" ").map{_.toInt}
            val a = Array.fill(m){readLine.toCharArray}
            val filler = new FloodFill {
                def conn: Array[(Int, Int)] = FloodFill.conn4
                def visit(x: Int, y: Int) {a(x)(y) = '#'}
                def sizeX = m
                def sizeY = n
                def admissible(x: Int, y: Int) = a(x)(y) == '-'
                def expandable(x: Int, y: Int) = true
            }
            var ans = 0
            for (i <- 0 until m)
                for (j <- 0 until n)
                    if (a(i)(j) == '-') {
                        ans += 1
                        filler.fill(i, j)
                    }
            println(s"Case $cases: $ans")
            cases += 1
        }
    }

    // Snippet: FloodFill
    object FloodFill {
        val conn4 = Array((0, 1), (-1, 0), (0, -1), (1, 0))
        val conn8 = Array(
            (0, 1), (-1, 1), (-1, 0), (-1, -1),
            (0, -1), (1, -1), (1, 0), (1, 1)
        )
    }

    abstract class FloodFill {
        import collection._

        def conn: Array[(Int, Int)]
        def visit(x: Int, y: Int): Unit
        def sizeX: Int
        def sizeY: Int
        def admissible(x: Int, y: Int): Boolean
        def expandable(x: Int, y: Int): Boolean

        def fill(x: Int, y: Int) {
            val q = new mutable.Queue[(Int, Int)]
            def meet(x: Int, y: Int) = {
                def xValid = 0 <= x && x < sizeX
                def yValid = 0 <= y && y < sizeY
                if (xValid && yValid && admissible(x, y)) {
                    visit(x, y)
                    if (expandable(x, y)) q.enqueue((x, y))
                }
            }
            meet(x, y)
            while (q.nonEmpty) {
                val (qHX, qHY) = q.dequeue
                for ((dx, dy) <- conn) meet(qHX + dx, qHY + dy)
            }
        }
    }
}
