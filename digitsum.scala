// Algorithm: Segment Tree
object digitsum {
    import io.StdIn._

    final def digitSum(n: Long, ans: Long = 0): Long =
        if (n == 0) ans else digitSum(n / 10, ans + n % 10)

    val cSum = Array.ofDim[Long](11)
    for (i <- 1 to 10) cSum(i) = cSum(i - 1) + (i - 1)

    final def digitSumC(n: Long, s: Long): Long = {
        if (n == 0L) return 0L
        val ans1 = (s - n % 10L) * (n % 10L)
        val ans2 = cSum((n % 10L).toInt)
        val ans3 = digitSumC(n / 10L, s - n % 10L) * 10L
        val ans4 = cSum(10) * (n / 10L)
        ans1 + ans2 + ans3 + ans4
    }

    final def digitSumC(n: Long): Long = digitSumC(n, digitSum(n))

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(a, b) = readLine.split(" ").map{_.toLong}
            println(digitSumC(b + 1L) - digitSumC(a))
        }
    }
}
