// Algorithm: None
object measurement {
    import io.StdIn._
    import collection._

    val len = new mutable.HashMap[String, Double]

    len.update("thou", 1.0)
    len.update("th", 1.0)

    def addUnit(full: String, abbr: String, amount: Int, relatedTo: String) {
        val inThou = len(relatedTo) * amount
        len.update(full, inThou)
        len.update(abbr, inThou)
    }

    addUnit("inch", "in", 1000, "thou")
    addUnit("foot", "ft", 12, "inch")
    addUnit("yard", "yd", 3, "foot")
    addUnit("chain", "ch", 22, "yard")
    addUnit("furlong", "fur", 10, "chain")
    addUnit("mile", "mi", 8, "furlong")
    addUnit("league", "lea", 3, "mile")

    def main(args: Array[String]) {
        val line = readLine.split(" ")
        println(line(0).toDouble * len(line(1)) / len(line(3)))
    }
}
