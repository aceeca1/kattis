// Algorithm: None
object timeloop {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        for (i <- 1 to n) println(s"$i Abracadabra")
    }
}
