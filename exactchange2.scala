// Algorithm: Dynamic Programming
object exactchange2 {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val price = readInt
            val n = readInt
            val a = Array.fill(n){readInt}
            var total = a.scanLeft(0){_ + _}.find{_ >= price}.get
            val w = Array.fill(total + 1){Int.MaxValue}
            w(0) = 0
            for (i <- a)
                for (j <- Range(total, i - 1, -1))
                    if (w(j - i) != Int.MaxValue)
                        w(j) = w(j) min w(j - i) + 1
            var ans = price
            while (w(ans) == Int.MaxValue) ans += 1
            println("%d %d".format(ans, w(ans)))
        }
    }
}
