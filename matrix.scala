// Algorithm: None
object matrix {
    import io.StdIn._

    def main(args: Array[String]) {
        var caseNo = 1
        while (true) {
            val line = readLine
            if (line == null) return
            val Array(a, b) = line.split(" ").map{_.toLong}
            val Array(c, d) = readLine.split(" ").map{_.toLong}
            readLine
            println(s"Case $caseNo:")
            val det = a * d - b * c
            println("%d %d".format(d / det, -b / det))
            println("%d %d".format(-c / det, a / det))
            caseNo += 1
        }
    }
}
