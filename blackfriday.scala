// Algorithm: None
object blackfriday {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}
        val b = Array.ofDim[Int](7)
        for (i <- a) b(i) += 1
        var m = 6
        while (m >= 0 && b(m) != 1) m -= 1
        if (m == -1) println("none")
        else println(a.indexOf(m) + 1)
    }
}
