// Algorithm: None
object vauvau {
    import io.StdIn._

    def dog(m: Int, a: Int, b: Int) = {
        val m1 = (m + a + b - 1) % (a + b)
        if (m1 < a) 1 else 0
    }

    def main(args: Array[String]) {
        val Array(a, b, c, d) = readLine.split(" ").map{_.toInt}
        for (i <- readLine.split(" ").map{_.toInt})
            println(dog(i, a, b) + dog(i, c, d) match {
                case 0 => "none"
                case 1 => "one"
                case 2 => "both"
            })
    }
}
