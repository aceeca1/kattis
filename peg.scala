// Algorithm: None
object peg {
    import io.StdIn._

    val legalMove1 = "oo\\.".r
    val legalMove2 = "\\.oo".r

    def legalM(a: Array[String]) = {
        val ans1 = a.map{legalMove1.findAllIn(_).size}.sum
        val ans2 = a.map{legalMove2.findAllIn(_).size}.sum
        ans1 + ans2
    }

    def main(args: Array[String]) {
        val a = Array.fill(7){readLine}
        val b = Array.tabulate(7){i => new String(a.map{_(i)})}
        println(legalM(a) + legalM(b))
    }
}
