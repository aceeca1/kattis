// Algorithm: Flood Fill
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <queue>
using namespace std;

vector<pair<int, int>> conn4{
    {0, 1}, {-1, 0}, {0, -1}, {1, 0}
};

vector<pair<int, int>> conn8{
    {0, 1}, {-1, 1}, {-1, 0}, {-1, -1},
    {0, -1}, {1, -1}, {1, 0}, {1, 1}
};

// Snippet: FloodFill
template <class Ops>
struct FloodFill {
    Ops&& ops;

    FloodFill(Ops&& ops_): ops(forward<Ops>(ops_)) {}

    void fill(int x, int y) {
        queue<pair<int, int>> q;
        auto meet = [&](int x, int y){
            if (
                0 <= x && x < ops.sizeX() &&
                0 <= y && y < ops.sizeY() &&
                ops.admissible(x, y)
            ) {
                ops.visit(x, y);
                if (ops.expandable(x, y)) q.emplace(x, y);
            }
        };
        meet(x, y);
        while (!q.empty()) {
            auto qH = q.front();
            q.pop();
            for (auto& i: ops.conn()) {
                int nX = qH.first + i.first;
                int nY = qH.second + i.second;
                meet(nX, nY);
            }
        }
    }
};

int main() {
    int r, c;
    scanf("%d%d ", &r, &c);
    vector<vector<char>> a(r, vector<char>(c));
    vector<vector<int>> b(r, vector<int>(c));
    for (int i = 0; i < r; ++i) {
        string s;
        getline(cin, s);
        for (int j = 0; j < c; ++j) {
            a[i][j] = s[j];
            b[i][j] = '0' - 1 - s[j];
        }
    }
    int no = 0;
    for (int iX = 0; iX < r; ++iX)
        for (int iY = 0; iY < c; ++iY)
            if (b[iX][iY] < 0) {
                struct Ops {
                    vector<vector<int>>& a;
                    int no, color;

                    vector<pair<int, int>>& conn() { return conn4; }
                    void visit(int x, int y) { a[x][y] = no; }
                    int sizeX() { return a.size(); }
                    int sizeY() { return a[0].size(); }
                    bool admissible(int x, int y) { return a[x][y] == color; }
                    bool expandable(int x, int y) { return true; }
                };
                FloodFill<Ops&&>(Ops{b, no++, b[iX][iY]}).fill(iX, iY);
            }
    int q;
    scanf("%d", &q);
    for (int iQ = 0; iQ < q; ++iQ) {
        int r1, c1, r2, c2;
        scanf("%d%d%d%d", &r1, &c1, &r2, &c2);
        --r1, --c1, --r2, --c2;
        if (b[r1][c1] != b[r2][c2]) printf("neither\n");
        else if (a[r1][c1] == '0') printf("binary\n");
        else printf("decimal\n");
    }
    return 0;
}
