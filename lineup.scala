// Algorithm: None
object lineup {
    import io.StdIn._

    def isSorted(a: Array[String])(f: (String, String) => Boolean) =
        (0).to(a.size - 2).forall{i => f(a(i), a(i + 1))}

    def main(args: Array[String]) {
        val a = Array.fill(readInt)(readLine)
        println(
            if (isSorted(a){_ < _}) "INCREASING"
            else if (isSorted(a){_ > _}) "DECREASING"
            else "NEITHER")
    }
}
