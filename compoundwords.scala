// Algorithm: None
object compoundwords {
    import io.StdIn._
    import collection.mutable.Buffer

    def readWords: Buffer[String] = {
        val ret = Buffer[String]()
        while (true) {
            val line = readLine
            if (line == null) return ret
            ret.appendAll(line.split(" "))
        }; null
    }

    def main(args: Array[String]) {
        val s = readWords
        val out = Buffer[String]()
        for (i <- s.indices)
            for (j <- i + 1 until s.size) {
                out.append(s(i) + s(j))
                out.append(s(j) + s(i))
            }
        for (i <- out.sorted.distinct) println(i)
    }
}
