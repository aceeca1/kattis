// Algorithm: None
object parking2 {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readInt
            val a = readLine.split(" ").map{_.toInt}
            println((a.max - a.min) << 1)
        }
    }
}
