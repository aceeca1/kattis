// Algorithm: None
object quiteaproblem {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val s = readLine
            if (s == null) return
            if (s.toLowerCase.contains("problem")) println("yes")
            else println("no")
        }
    }
}
