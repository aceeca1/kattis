// Algorithm: None
object communication {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = Array.ofDim[Int](256)
        for (i <- 0 until 256) a((i ^ (i << 1)) & 255) = i
        val n = readInt
        println(readLine.split(" ").map{k => a(k.toInt)}.mkString(" "))
    }
}
