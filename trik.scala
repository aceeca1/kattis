// Algorithm: None
object trik {
    import io.StdIn._

    val tr = Array(Array(1, 0, 2), Array(0, 2, 1), Array(2, 1, 0))

    def main(args: Array[String]) {
        var ball = 0
        for (i <- readLine) ball = tr(i - 'A')(ball)
        println(ball + 1)
    }
}
