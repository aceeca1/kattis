// Algorithm: None
object doorman {
    import io.StdIn._

    def solve(maxDiff: Int, s: String): Int = {
        var ans = 0
        var c = 'U'
        var b = 0
        for (i <- s) c match {
            case 'U' => c = i
            case 'M' =>
                if (b == maxDiff)  i match {
                    case 'M' => return ans
                    case 'W' => ans += 1; b -= 1
                } else { ans += 1; b += 1; c = i }
            case 'W' =>
                if (b == -maxDiff) i match {
                    case 'W' => return ans
                    case 'M' => ans += 1; b += 1
                } else { ans += 1; b -= 1; c = i }
        }
        c match {
            case 'U' => ans
            case 'M' => if (b == maxDiff)  ans else ans + 1
            case 'W' => if (b == -maxDiff) ans else ans + 1
        }
    }

    def main(args: Array[String]) {
        println(solve(readInt, readLine))
    }
}
