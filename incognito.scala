// Algorithm: None
object incognito {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val n = readInt
            val a = Array.fill(n){readLine.split(" ")(1)}
            println(a.groupBy{identity}.map{
                case (k, v) => v.size + 1
            }.product - 1)
        }
    }
}
