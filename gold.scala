// Algorithm: Flood Fill
object gold {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(w, h) = readLine.split(" ").map{_.toInt}
        val a = Array.fill(h){readLine.toCharArray}
        var numGold = 0
        val ff = new FloodFill {
            def conn: Array[(Int, Int)] = FloodFill.conn4
            def visit(x: Int, y: Int) {
                if (a(x)(y) == 'G') numGold += 1
                a(x)(y) = '#'
            }
            def sizeX = h
            def sizeY = w
            def admissible(x: Int, y: Int) = a(x)(y) match {
                case 'P' | '.' | 'G' => true
                case _ => false
            }
            def expandable(x: Int, y: Int): Boolean = {
                if (0 < x && a(x - 1)(y) == 'T') return false
                if (0 < y && a(x)(y - 1) == 'T') return false
                if (x < sizeX - 1 && a(x + 1)(y) == 'T') return false
                if (y < sizeY - 1 && a(x)(y + 1) == 'T') return false
                return true
            }
        }
        val pX = a.map{_.indexOf('P')}.indexWhere{_ != -1}
        val pY = a.map{_.indexOf('P')}.find{_ != -1}.get
        ff.fill(pX, pY)
        println(numGold)
    }

    // Snippet: FloodFill
    object FloodFill {
        val conn4 = Array((0, 1), (-1, 0), (0, -1), (1, 0))
        val conn8 = Array(
            (0, 1), (-1, 1), (-1, 0), (-1, -1),
            (0, -1), (1, -1), (1, 0), (1, 1)
        )
    }

    abstract class FloodFill {
        import collection._

        def conn: Array[(Int, Int)]
        def visit(x: Int, y: Int): Unit
        def sizeX: Int
        def sizeY: Int
        def admissible(x: Int, y: Int): Boolean
        def expandable(x: Int, y: Int): Boolean

        def fill(x: Int, y: Int) {
            val q = new mutable.Queue[(Int, Int)]
            def meet(x: Int, y: Int) = {
                def xValid = 0 <= x && x < sizeX
                def yValid = 0 <= y && y < sizeY
                if (xValid && yValid && admissible(x, y)) {
                    visit(x, y)
                    if (expandable(x, y)) q.enqueue((x, y))
                }
            }
            meet(x, y)
            while (q.nonEmpty) {
                val (qHX, qHY) = q.dequeue
                for ((dx, dy) <- conn) meet(qHX + dx, qHY + dy)
            }
        }
    }
}
