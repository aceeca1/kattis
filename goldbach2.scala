// Algorithm: Prime Sieve
object goldbach2 {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        val a = Array.fill(readInt){readInt}
        val prime = new Prime(a.max)
        for (n <- a) {
            val buf = Buffer[Int]()
            var i = 2
            while (i <= n - i) {
                val k = n - i
                if (prime.is(k)) buf.append(i)
                i = prime.next(i)
            }
            println(s"$n has ${buf.size} representation(s)")
            for (i <- buf) println(s"$i+${n - i}")
            println
        }
    }

    // Snippet: Prime
    class Prime(n: Int) {
        import collection.mutable.Buffer

        val a = Array.ofDim[Int](n + 1)

        {
            var u = 0
            for (i <- 2 to n) {
                var v = a(i)
                if (v == 0) { v = i; a(u) = i; u = i }
                var w = 2
                while (w > 0 && i * w <= n) {
                    a(i * w) = w
                    if (w >= v) w = -1 else w = a(w)
                }
            }
            a(u) = n + 1
        }

        def is(n: Int) = n >= 2 && a(n) > n
        def next(n: Int) = a(n)

        def toBuffer = {
            val ret = Buffer[Int]()
            var i = 2
            while (i <= n) {
                ret.append(i)
                i = a(i)
            }
            ret
        }
    }
}
