// Algorithm: None
object piglatin {
    import io.StdIn._

    val vowel = "aeiouy".toSet

    def pig(s: String) = {
        val k = s.indexWhere(vowel)
        if (k == 0) s + "yay"
        else s.substring(k) + s.substring(0, k) + "ay"
    }

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            println(line.split(" ").map(pig).mkString(" "))
        }
    }
}
