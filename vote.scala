// Algorithm: None
object vote {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val a = Array.fill(readInt){readInt}
            val m = a.max
            val s = a.sum
            val idx = a.indices.filter{i => a(i) == m}
            if (idx.size > 1) println("no winner")
            else if (m + m > s) println(s"majority winner ${idx(0) + 1}")
            else println(s"minority winner ${idx(0) + 1}")
        }
    }
}
