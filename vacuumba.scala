// Algorithm: None
object vacuumba {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            var x, y = 0.0
            var angle = 90.0
            for (_ <- 1 to readInt) {
                val Array(a, len) = readLine.split(" ").map{_.toDouble}
                angle += a
                x += Math.cos(angle * (Math.PI / 180.0)) * len
                y += Math.sin(angle * (Math.PI / 180.0)) * len
            }
            println(s"$x $y")
        }
    }
}
