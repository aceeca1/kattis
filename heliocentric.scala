// Algorithm: None
object heliocentric {
    import io.StdIn._

    def main(args: Array[String]) {
        var caseNo = 1
        while (true) {
            val line = readLine
            if (line == null) return
            val Array(e, m) = line.split(" ").map{_.toInt}
            val ans = (e * 11679 + m * 239075) % (365 * 687)
            println(s"Case $caseNo: $ans")
            caseNo += 1
        }
    }
}
