// Algorithm: Dynamic Programming
object commercials {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, p) = readLine.split(" ").map{_.toInt}
        val a = readLine.split(" ").map{_.toInt}
        for (i <- a.indices) a(i) -= p
        var ans = 0
        var pV = 0
        for (i <- a.indices) {
            val cV = a(i) + (pV max 0)
            if (cV > ans) ans = cV
            pV = cV
        }
        println(ans)
    }
}
