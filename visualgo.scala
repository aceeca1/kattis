// Algorithm: Shortest Path
object visualgo {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        val Array(v, e) = readLine.split(" ").map{_.toInt}
        val g = Array.fill(v){Buffer[(Int, Int)]()}
        for (_ <- 0 until e) {
            val Array(s, t, c) = readLine.split(" ").map{_.toInt}
            g(s).append((t, c))
        }
        val Array(s, t) = readLine.split(" ").map{_.toInt}
        val sp = new ShortPath[Int] {
            def sizeV = g.size
            def e(no: Int) = g(no).toIterator
            def src = s
            def infinity = Int.MaxValue
        }
        val numPaths = Array.fill(g.size){-1}
        numPaths(t) = 1
        def cNumPaths(no: Int): Int = {
            if (numPaths(no) != -1) return numPaths(no)
            val list =
                for ((t, c) <- g(no); if (sp.dis(no) + c == sp.dis(t)))
                    yield cNumPaths(t)
            val ans = list.sum
            numPaths(no) = ans
            ans
        }
        println(cNumPaths(s))
    }

    // Snippet: ShortPath
    abstract class ShortPath[T: reflect.ClassTag](implicit num: Numeric[T]) {
        import collection._
        import collection.mutable.Buffer
        import num._

        def sizeV: Int
        def e(no: Int): Iterator[(Int, T)]
        def src: Int
        def infinity: T

        val dis = Array.fill(sizeV){infinity}
        val from = Array.ofDim[Int](sizeV)

        {
            val q = new java.util.ArrayDeque[Int]
            val inQ = Array.ofDim[Boolean](sizeV)
            dis(src) = zero
            q.add(src)
            inQ(src) = true
            while (!q.isEmpty) {
                val qH = q.poll
                inQ(qH) = false
                for ((t, c) <- e(qH)) {
                    val newDis = dis(qH) + c
                    if (newDis < dis(t)) {
                        dis(t) = newDis
                        from(t) = qH
                        if (!inQ(t)) {
                            if (!q.isEmpty && newDis < dis(q.peek))
                                q.addFirst(t)
                            else
                                q.addLast(t)
                            inQ(t) = true
                        }
                    }
                }
            }
        }

        def route(dst: Int) = {
            val ans = Buffer[Int]()
            var no = dst
            while (no != src) {
                ans.append(no)
                no = from(no)
            }
            ans.append(src)
            ans.reverse
        }
    }
}
