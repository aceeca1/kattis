// Algorithm: None
object securedoors {
    import io.StdIn._
    import collection._

    val reEntry = "entry (.*)".r
    val reExit = "exit (.*)".r

    def main(args: Array[String]) {
        val a = mutable.HashSet[String]()
        for (_ <- 1 to readInt) readLine match {
            case reEntry(name) =>
                print(s"$name entered")
                if (!a.add(name)) println(" (ANOMALY)")
                else println
            case reExit(name) =>
                print(s"$name exited")
                if (!a.remove(name)) println(" (ANOMALY)")
                else println
        }
    }
}
