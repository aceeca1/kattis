// Algorithm: None
object herman {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readDouble
        println(n * n * Math.PI)
        println(n * n * 2)
    }
}
