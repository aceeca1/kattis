// Algorithm: None
object howmanydigits {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            println(line.toInt match {
                case 0 | 1 | 2 | 3 => 1
                case n =>
                    val ans1 = Math.log(Math.PI * 2.0) * 0.5
                    val ans2 = Math.log(n) * (n + 0.5)
                    val ans = (ans1 + ans2 - n) / Math.log(10.0)
                    Math.floor(ans).toInt + 1
            })
        }
    }
}
