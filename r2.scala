// Algorithm: None
object r2 {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(r1, s) = readLine.split(" ").map{_.toInt}
        println(s + s - r1)
    }
}
