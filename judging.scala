// Algorithm: None
object judging {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a1 = Array.fill(n){readLine}
        val a2 = Array.fill(n){readLine}
        println(a1.intersect(a2).size)
    }
}
