// Algorithm: None
object welcomehard {
    import io.StdIn._
    import collection.mutable.Buffer

    val welcome = "welcome to code jam"
    val a = Array.fill(256){Buffer[Int]()}
    for (i <- welcome.indices) a(welcome(i)).append(i)

    def main(args: Array[String]) {
        for (caseNo <- 1 to readInt) {
            val b = Array.fill(welcome.size + 1){0}
            b(0) = 1
            for (i <- readLine) {
                for (j <- a(i)) b(j + 1) = (b(j + 1) + b(j)) % 10000
            }
            println("Case #%d: %04d".format(caseNo, b.last))
        }
    }
}
