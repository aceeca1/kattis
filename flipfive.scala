// Algorithm: None
object flipfive {
    import io.StdIn._

    val adjacent = Array.fill(9){0}

    for (ix <- 0 until 3)
        for (iy <- 0 until 3)
            for ((dx, dy) <- Array((0, 0), (0, 1), (0, -1), (1, 0), (-1, 0))) {
                val ax = ix + dx
                val ay = iy + dy
                if (0 <= ax && ax < 3 && 0 <= ay && ay < 3)
                    adjacent(ix * 3 + iy) |= 1 << (ax * 3 + ay)
            }

    def main(args: Array[String]) {
        val a = Array.fill(1 << 9){Int.MaxValue}
        for (i <- 0 until 1 << 9) {
            val bc = Integer.bitCount(i)
            val m = (0 until 9).filter{
                k => ((i >>> k) & 1) != 0
            }.map(adjacent).fold(0){_ ^ _}
            if (bc < a(m)) a(m) = bc
        }
        for (_ <- 1 to readInt)
            println(a((0 until 3).flatMap{i => readLine}.map{k =>
                if (k == '*') 1 else 0
            }.reduce{(a1, a2) => a1 + a1 + a2}))
    }
}
