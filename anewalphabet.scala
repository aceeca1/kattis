// Algorithm: None
object anewalphabet {
    import io.StdIn._

    val tr = Array(
        "@", "8", "(", "|)", "3", "#", "6",
        "[-]", "|", "_|", "|<", "1", "[]\\/[]",
        "[]\\[]", "0", "|D", "(,)", "|Z", "$", "']['",
        "|_|", "\\/", "\\/\\/", "}{", "`/", "2")

    def main(args: Array[String]) {
        println(readLine.flatMap{i => {
            val iL = i.toLower
            if (iL.isLower) tr(iL - 'a') else iL.toString
        }})
    }
}
