// Algorithm: None
object towering {
    import io.StdIn._

    def solve(a: Array[Int], sum1: Int): String = {
        for {
            i1 <- 0 to 5
            i2 <- i1 + 1 to 5
            i3 <- i2 + 1 to 5
            if (a(i1) + a(i2) + a(i3) == sum1)
        } {
            val b = Array.ofDim[Boolean](6)
            b(i1) = true
            b(i2) = true
            b(i3) = true
            val Seq(j1, j2, j3) = (0 to 5).filter{j => !b(j)}
            return Array(
                a(i3), a(i2), a(i1),
                a(j3), a(j2), a(j1)
            ).mkString(" ")
        }; null
    }

    def main(args: Array[String]) {
        val line = readLine.split(" ").map{_.toInt}
        val a = line.slice(0, 6).sorted
        val sum1 = line(6)
        println(solve(a, sum1))
    }
}
