// Algorithm: None
object bishops {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            println(line.toInt match {
                case 1 => 1
                case n => n + n - 2
            })
        }
    }
}
