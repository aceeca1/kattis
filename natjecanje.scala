// Algorithm: None
object natjecanje {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, s, r) = readLine.split(" ").map{_.toInt}
        val damaged = readLine.split(" ").map{_.toInt}.sorted
        val reserve = readLine.split(" ").map{_.toInt}.sorted
        var k = 0
        def failed(no: Int): Boolean = {
            if (k == reserve.size) return true
            val resK = reserve(k)
            val near = no == resK - 1 || no == resK || no == resK + 1
            if (near) k += 1
            !near
        }
        println(damaged.count(failed))
    }
}
