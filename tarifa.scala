// Algorithm: None
object tarifa {
    import io.StdIn._

    def main(args: Array[String]) {
        val x = readInt
        val n = readInt
        println(x * (n + 1) - Array.fill(n){readInt}.sum)
    }
}
