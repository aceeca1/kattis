// Algorithm: None
object estimatingtheareaofacircle {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val Array(r, m, c) = readLine.split(" ").map{_.toDouble}
            if (r == 0.0) return
            val ans1 = Math.PI * r * r
            val ans2 = r * r * 4.0 * c / m
            println(s"$ans1 $ans2")
        }
    }
}
