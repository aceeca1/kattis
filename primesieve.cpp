// Algorithm: Prime Sieve
#include <cstdio>
#include <vector>
#include <algorithm>
#include <cmath>
#include <unordered_map>
using namespace std;

template <class Func>
void sieve(vector<bool>& u, vector<bool>& v, int start, Func&& f) {
    for (int i = 2; i < u.size(); ++i) if (!u[i]) {
        int lb1 = i * i;
        int lb2 = (start + i - 1) / i * i;
        int lb = max(lb1, lb2);
        int ub = start + v.size();
        for (int j = lb; j < ub; j += i) v[j - start] = true;
    }
    for (int i = 0; i < v.size(); ++i) {
        int num = start + i;
        if (num >= 2 && !v[i]) f(num);
    }
}

int main() {
    int n, q;
    scanf("%d%d", &n, &q);
    unordered_map<int, int> b;
    for (int i = 0; i < q; ++i) {
        int ai;
        scanf("%d", &ai);
        b[ai] = i;
    }
    vector<bool> a(q);
    int ans = 0;
    auto op = [&](int prime) {
        if (prime <= n) ++ans;
        auto bp = b.find(prime);
        if (bp != b.end()) a[bp->second] = true;
    };
    int m = sqrt(n + 1) + 1.0;
    vector<bool> u(m);
    sieve(u, u, 0, op);
    for (int i = 1; i < m; ++i) {
        vector<bool> v(m);
        sieve(u, v, i * m, op);
    }
    printf("%d\n", ans);
    for (int i = 0; i < q; ++i) printf("%d\n", int(a[i]));
    return 0;
}
