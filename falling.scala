// Algorithm: None
object falling {
    import io.StdIn._

    def main(args: Array[String]) {
        val d = readInt
        if ((d & 3) == 0) {
            val k = d >> 2
            println("%d %d".format(k - 1, k + 1))
        } else if ((d & 1) != 0) {
            val k = d >> 1
            println("%d %d".format(k, k + 1))
        } else println("impossible")
    }
}
