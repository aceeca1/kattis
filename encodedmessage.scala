// Algorithm: None
object encodedmessage {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val s = readLine
            val n = Math.sqrt(s.size).toInt
            var k = 0
            val a = Array.fill(n, n){
                k += 1
                s(k - 1)
            }
            val b = new StringBuilder
            for (i <- Range(n - 1, -1, -1))
                for (j <- 0 until n) b.append(a(j)(i))
            println(b)
        }
    }
}
