// Algorithm: None
object beavergnaw {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val Array(d, v) = readLine.split(" ").map{_.toDouble}
            if (d == 0.0) return
            println(Math.cbrt(d * d * d - 6.0 / Math.PI * v))
        }
    }
}
