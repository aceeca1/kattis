// Algorithm: None
object gamerank {
    import io.StdIn._

    val maxStar = Array(
        Int.MaxValue,
        5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
        4, 4, 4, 4, 4, 3, 3, 3, 3, 3,
        2, 2, 2, 2, 2
    )

    case class Status(var rank: Int, var star: Int, var winning: Int) {
        def win {
            winning += 1
            val newStar = if (6 <= rank && winning >= 3) 2 else 1
            star += newStar
            if (star > maxStar(rank)) {
                star -= maxStar(rank)
                rank -= 1
            }
        }

        def lose {
            winning = 0
            val newStar = if (1 <= rank && rank <= 20) -1 else 0
            star += newStar
            if (rank == 20 && star < 0) star = 0
            if (star < 0) {
                star += maxStar(rank + 1)
                rank += 1
            }
        }
    }

    def main(args: Array[String]) {
        val player = Status(25, 0, 0)
        for (i <- readLine) i match {
            case 'W' => player.win
            case 'L' => player.lose
        }
        player.rank match {
            case 0 => println("Legend")
            case n => println(n)
        }
    }
}
