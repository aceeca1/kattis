// Algorithm: None
object savingdaylight {
    import io.StdIn._

    case class Time(h: Int, m: Int) {
        def -(that: Time) = {
            var h = this.h - that.h
            var m = this.m - that.m
            if (m < 0) { m += 60; h -= 1 }
            Time(h, m)
        }
    }

    def parseTime(s: String) = {
        val Array(h, m) = s.split(":").map{_.toInt}
        Time(h, m)
    }

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            val lineS = line.split(" ")
            val sunrise = parseTime(lineS(3))
            val sunset = parseTime(lineS(4))
            val sun = sunset - sunrise
            println("%s %s %s %d hours %d minutes".format(
                lineS(0), lineS(1), lineS(2), sun.h, sun.m))
        }
    }
}
