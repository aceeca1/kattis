// Algorithm: None
object cetvrta {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(x1, y1) = readLine.split(" ").map{_.toInt}
        val Array(x2, y2) = readLine.split(" ").map{_.toInt}
        val Array(x3, y3) = readLine.split(" ").map{_.toInt}
        println("%d %d".format(x1 ^ x2 ^ x3, y1 ^ y2 ^ y3))
    }
}
