// Algorithm: None
object areal {
    import io.StdIn._

    def main(args: Array[String]) {
        println(Math.sqrt(readDouble) * 4.0)
    }
}
