// Algorithm: None
object flexible {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(w, p) = readLine.split(" ").map{_.toInt}
        val a = readLine.split(" ").map{_.toInt}
        val b = Array.concat(Array(0), a, Array(w))
        val c = Array.ofDim[Boolean](w + 1)
        for (i <- b.indices)
            for (j <- i + 1 until b.size)
                c(b(j) - b(i)) = true
        println(c.indices.filter(c).mkString(" "))
    }
}
