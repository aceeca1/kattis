// Algorithm: None
object pet {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = Array.fill(5){readLine.split(" ").map{_.toInt}.sum}
        val k = a.indices.maxBy{i => a(i)}
        println("%d %d".format(k + 1, a(k)))
    }
}
