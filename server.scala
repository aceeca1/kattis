// Algorithm: None
object server {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(n, t) = readLine.split(" ").map{_.toInt}
        val a = readLine.split(" ").map{_.toInt}
        def f(m: Int, t: Int): Int = () match {
            case _ if m == n || a(m) > t => m
            case _ => f(m + 1, t - a(m))
        }
        println(f(0, t))
    }
}
