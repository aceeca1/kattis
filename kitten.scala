// Algorithm: None
object kitten {
    import io.StdIn._

    def readTree: Array[Int] = {
        val ret = Array.ofDim[Int](101)
        while (true) {
            val line = readLine.split(" ").map{_.toInt}
            if (line(0) == -1) return ret
            for (i <- line.tail) ret(i) = line(0)
        }; null
    }

    def main(args: Array[String]) {
        var k = readInt
        val tree = readTree
        var head = true
        while (k != 0) {
            if (!head) print(' ')
            head = false
            print(k)
            k = tree(k)
        }
        println
    }
}
