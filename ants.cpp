// Algorithm: None
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int t;
    scanf("%d", &t);
    while (t--) {
        int l, n;
        scanf("%d%d", &l, &n);
        vector<int> b(n);
        for (int i = 0; i < n; ++i) scanf("%d", &b[i]);
        int ans1 = 0, ans2 = 0;
        for (int i = 0; i < n; ++i) {
            int a1 = b[i];
            int a2 = l - b[i];
            int mn = min(a1, a2);
            if (mn > ans1) ans1 = mn;
            int mx = max(a1, a2);
            if (mx > ans2) ans2 = mx;
        }
        printf("%d %d\n", ans1, ans2);
    }
    return 0;
}
