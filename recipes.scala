// Algorithm: None
object recipes {
    import io.StdIn._

    def main(args: Array[String]) {
        for (caseNo <- 1 to readInt) {
            val Array(r, p, d) = readLine.split(" ").map{_.toInt}
            val a = Array.fill(r){
                val line = readLine.split(" ")
                val name = line(0)
                val weight = line(1).toDouble
                val percent = line(2).toDouble
                (name, weight, percent)
            }
            val scaleFactor = d.toDouble / p
            val mainWeight = a.find{_._3 == 100.0}.get._2 * scaleFactor
            println(s"Recipe # $caseNo")
            for ((name, _, percent) <- a) {
                val weight = percent / 100.0 * mainWeight
                println("%s %.1f".format(name, weight))
            }
            println("----------------------------------------")
        }
    }
}
