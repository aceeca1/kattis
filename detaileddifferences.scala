// Algorithm: None
object detaileddifferences {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val s1 = readLine
            val s2 = readLine
            println(s1)
            println(s2)
            println(Iterator.tabulate(s1.size){i =>
                if (s1(i) == s2(i)) '.' else '*'
            }.mkString)
        }
    }
}
