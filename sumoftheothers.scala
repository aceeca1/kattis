// Algorithm: None
object sumoftheothers {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            println(line.split(" ").map{_.toInt}.sum / 2)
        }
    }
}
