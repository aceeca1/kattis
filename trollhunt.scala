// Algorithm: None
object trollhunt {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(b, k, g) = readLine.split(" ").map{_.toInt}
        def divUpper(a1: Int, a2: Int) = (a1 + a2 - 1) / a2
        println(divUpper(b - 1, k / g))
    }
}
