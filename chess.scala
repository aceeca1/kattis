// Algorithm: None
object chess {
    import io.StdIn._

    def toZW(x: Char, y: Char) = {
        val x0 = x - 'A'
        val y0 = y - '1'
        (x0 + y0, x0 - y0)
    }

    def toXY(z: Int, w: Int) = {
        val x = (z + w) >> 1
        val y = (z - w) >> 1
        def xValid = 0 <= x && x <= 7
        def yValid = 0 <= y && y <= 7
        if (xValid && yValid) (('A' + x).toChar, ('1' + y).toChar) else null
    }

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(sX, sY, tX, tY) = readLine.split(" ").map{_(0)}
            if (sX == tX && sY == tY) println(s"0 $sX $sY")
            else {
                val sK = (sX + sY) & 1
                val tK = (tX + tY) & 1
                if (sK != tK) println("Impossible")
                else {
                    val (sZ, sW) = toZW(sX, sY)
                    val (tZ, tW) = toZW(tX, tY)
                    val via1 = toXY(sZ, tW)
                    val via2 = toXY(tZ, sW)
                    val via = if (via1 != null) via1 else via2
                    if (via == (sX, sY) || via == (tX, tY))
                        println(s"1 $sX $sY $tX $tY")
                    else {
                        val (viaX, viaY) = via
                        println(s"2 $sX $sY $viaX $viaY $tX $tY")
                    }
                }
            }
        }
    }
}
