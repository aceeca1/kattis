// Algorithm: None
object falsesecurity {
    import io.StdIn._
    import collection._

    val morse1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_,.?"
    val morse2 = Array(
        ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
        "....", "..", ".---", "-.-", ".-..", "--", "-.",
        "---", ".--.", "--.-", ".-.", "...", "-", "..-",
        "...-", ".--", "-..-", "-.--", "--..",
        "..--", ".-.-", "---.", "----"
    )
    val toMorse = Array.ofDim[String](256)
    val fromMorse = new mutable.HashMap[String, Char]
    for (i <- morse1.indices) {
        toMorse(morse1(i)) = morse2(i)
        fromMorse(morse2(i)) = morse1(i)
    }

    def encode(s: String) = {
        val morse = new StringBuilder
        val len = Array.ofDim[Int](s.size)
        for (i <- s.indices) {
            val m = toMorse(s(i))
            morse.append(m)
            len(i) = m.size
        }
        (morse.toString, len)
    }

    def decode(morse: String, len: Array[Int]) = {
        var k = 0
        new String(Array.tabulate(len.size){i =>
            val m = morse.substring(k, k + len(i))
            k += len(i)
            fromMorse(m)
        })
    }

    def main(args: Array[String]) {
        while (true) {
            val s = readLine
            if (s == null) return
            val (morse, len) = encode(s)
            println(decode(morse, len.reverse))
        }
    }
}
