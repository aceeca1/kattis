// Algorithm: Deep First Search
object wheresmyinternet {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        val Array(n, m) = readLine.split(" ").map{_.toInt}
        val g = Array.fill(n + 1){Buffer[Int]()}
        for (_ <- 0 until m) {
            val Array(v1, v2) = readLine.split(" ").map{_.toInt}
            g(v1).append(v2)
            g(v2).append(v1)
        }
        val cc = new ConnectedComponent {
            def sizeV = g.size
            def e(no: Int) = g(no).toIterator
        }
        var connected = true
        for (i <- 1 to n) if (cc.label(i) != cc.label(1)) {
            println(i)
            connected = false
        }
        if (connected) println("Connected")
    }

    // Snippet: ConnectedComponent
    abstract class ConnectedComponent {
        import collection._
        import collection.mutable.Buffer

        def sizeV: Int
        def e(no: Int): Iterator[Int]

        val label = Array.fill(sizeV){-1}
        var num = 0

        def visit(no: Int) {
            label(no) = num
            for (t <- e(no)) if (label(t) == -1) visit(t)
        }

        for (i <- 0 until sizeV)
            if (label(i) == -1) { visit(i); num += 1 }
    }
}
