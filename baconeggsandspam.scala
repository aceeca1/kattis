// Algorithm: None
object baconeggsandspam {
    import io.StdIn._
    import collection.mutable.Buffer

    case class Like(food: String, man: String)

    def main(args: Array[String]) {
        while (true) {
            val n = readInt
            if (n == 0) return
            val a = Buffer[Like]()
            for (i <- 1 to n) {
                val b = readLine.split(" ")
                for (i <- b.tail) a.append(Like(i, b.head))
            }
            new PartitionBy[Like, String] {
                val in = a.sortBy(Like.unapply)
                def eqv(a1: Like, a2: Like) = a1.food == a2.food
                def mapTo = new Aggregator[Like, String] {
                    val sb = new StringBuilder
                    def add(data: Like) {
                        if (sb.isEmpty) sb.append(data.food)
                        sb.append(' ').append(data.man)
                    }
                    def result = sb.toString
                }
            }.foreach(println)
            println
        }
    }

    // Snippet: PartitionBy
    abstract class Aggregator[T, U] {
        def add(data: T): Unit
        def result: U
    }

    abstract class PartitionBy[T, U] extends Traversable[U] {
        def in: Traversable[T]
        def eqv(a1: T, a2: T): Boolean
        def mapTo: Aggregator[T, U]

        def foreach[V](f: U => V) {
            var data = null.asInstanceOf[T]
            var aggr: Aggregator[T, U] = null
            for (i <- in)
                if (aggr == null) {
                    data = i
                    aggr = mapTo
                    aggr.add(i)
                } else if (eqv(data, i)) {
                    data = i
                    aggr.add(i)
                } else {
                    f(aggr.result)
                    data = i
                    aggr = mapTo
                    aggr.add(i)
                }
            if (aggr != null) f(aggr.result)
        }
    }
}
