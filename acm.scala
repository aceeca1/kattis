// Algorithm: None
object acm {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = Array.ofDim[Int](26)
        var ans1 = 0
        var ans2 = 0
        while (true) {
            val line = readLine
            if (line == "-1") {
                println("%d %d".format(ans1, ans2))
                return
            }
            val Array(time, prob, stat) = line.split(" ")
            val probI = prob(0) - 'A'
            if (a(probI) >= 0) stat match {
                case "right" =>
                    ans1 += 1
                    ans2 += a(probI) + time.toInt
                    a(probI) = -1
                case "wrong" => a(probI) += 20
            }
        }
    }
}
