// Algorithm: None
object cold {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val a = readLine.split(" ").map{_.toInt}
        println(a.count{_ < 0})
    }
}
