// Algorithm: None
object virus {
    import io.StdIn._

    def main(args: Array[String]) {
        val s1 = readLine
        val s2 = readLine
        val z = s1.size min s2.size
        var i1 = 0
        while (i1 < z && s1(i1) == s2(i1)) i1 += 1
        var i2 = 0
        while (i2 < z && s1(s1.size - 1 - i2) == s2(s2.size - 1 - i2)) i2 += 1
        println(s2.size - (i1 + i2 min z))
    }
}
