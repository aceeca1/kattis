// Algorithm: None
object volim {
    import io.StdIn._

    def main(args: Array[String]) {
        var k = readInt
        var total = 210
        Iterator.fill(readInt){
            val line = readLine.split(" ")
            val t = line(0).toInt
            val z = line(1)(0)
            (t, z)
        }.takeWhile{case (t, z) =>
            total -= t
            total >= 0
        }.foreach{i => if (i._2 == 'T') k += 1}
        println(((k + 7) & 7) + 1)
    }
}
