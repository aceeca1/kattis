// Algorithm: Floyd Warshall
object secretchamber {
    import io.StdIn._

    def main(args: Array[String]) {
        val Array(m, n) = readLine.split(" ").map{_.toInt}
        val a = Array.ofDim[Boolean](26, 26)
        for (i <- 0 until 26) a(i)(i) = true
        for (_ <- 0 until m) {
            val Array(c1, c2) = readLine.split(" ").map{_(0)}
            a(c1 - 'a')(c2 - 'a') = true
        }
        new FloydWarshall {
            def sizeV = a.size
            def e(a1: Int, a2: Int) = a(a1)(a2)
            def eSet(a1: Int, a2: Int) { a(a1)(a2) = true }
        }
        for (_ <- 0 until n) {
            val Array(s1, s2) = readLine.split(" ")
            def b1 = s1.size == s2.size
            def b2 = s1.indices.forall{i => a(s1(i) - 'a')(s2(i) - 'a')}
            if (b1 && b2) println("yes")
            else println("no")
        }
    }

    // Snippet: FloydWarshall
    abstract class FloydWarshall {
        def sizeV: Int
        def e(a1: Int, a2: Int): Boolean
        def eSet(a1: Int, a2: Int): Unit

        for (i <- 0 until sizeV)
            for (j <- 0 until sizeV)
                for (k <- 0 until sizeV)
                    if (e(j, i) && e(i, k)) eSet(j, k)
    }
}
