// Algorithm: None
object pervasiveheartmonitor {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            var n = 0
            var s = 0.0
            val sb = new StringBuilder
            for (i <- line.split(" "))
                if (i(0).isLetter) {
                    sb.append(' ')
                    sb.append(i)
                } else {
                    n += 1
                    s += i.toDouble
                }
            println(s"${s / n}$sb")
        }
    }
}
