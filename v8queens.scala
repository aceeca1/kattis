// Algorithm: None
object v8queens {
    import io.StdIn._

    def checkH(a: Array[String]) =
        a.forall{ai => ai.count{k => k == '*'} == 1}

    def checkV(a: Array[String]) =
        (0 until 8).forall{i => a.count{ak => ak(i) == '*'} <= 1}

    def checkM(a: Array[String]) =
        (-7 to 7).forall{i =>
            (0 until 8).count{k =>
                val k1 = i + k
                0 <= k1 && k1 <= 7 && a(k)(k1) == '*'} <= 1}

    def checkA(a: Array[String]) =
        (0 to 14).forall{i =>
            (0 until 8).count{k =>
                val k1 = i - k
                0 <= k1 && k1 <= 7 && a(k)(k1) == '*'} <= 1}

    def main(args: Array[String]) {
        val a = Array.fill(8)(readLine)
        if (checkH(a) && checkV(a) && checkM(a) && checkA(a)) println("valid")
        else println("invalid")
    }
}
