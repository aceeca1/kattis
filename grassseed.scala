// Algorithm: None
object grassseed {
    import io.StdIn._

    def main(args: Array[String]) {
        val c = readDouble
        println(Iterator.fill(readInt){
            readLine.split(" ").map{_.toDouble}.product
        }.sum * c)
    }
}
