// Algorithm: None
object ferryloading4 {
    import io.StdIn._
    import collection.mutable.Buffer

    final def shuttle(
        maxLoad: Int, ans: Int,
        left: Buffer[Int], leftK: Int,
        right: Buffer[Int], rightK: Int
    ): Int = {
        if (left.size == leftK && right.size == rightK) return ans
        var leftKM = leftK
        var load = 0
        while (leftKM < left.size && load + left(leftKM) < maxLoad) {
            load += left(leftKM)
            leftKM += 1
        }
        shuttle(maxLoad, ans + 1, right, rightK, left, leftKM)
    }

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(l, m) = readLine.split(" ").map{_.toInt}
            val maxLoad = l * 100
            val left = Buffer[Int]()
            val right = Buffer[Int]()
            for (_ <- 0 until m) {
                val line = readLine.split(" ")
                val car = line(0).toInt
                line(1) match {
                    case "left" => left.append(car)
                    case "right" => right.append(car)
                }
            }
            println(shuttle(maxLoad, 0, left, 0, right, 0))
        }
    }
}
