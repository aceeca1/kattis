// Algorithm: Shortest Path
object shortestpath1 {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        while (true) {
            val Array(n, m, q, s) = readLine.split(" ").map{_.toInt}
            if (n == 0) return
            val g = Array.fill(n){Buffer[(Int, Int)]()}
            for (_ <- 0 until m) {
                val Array(u, v, w) = readLine.split(" ").map{_.toInt}
                g(u).append((v, w))
            }
            val sp = new ShortPath[Int] {
                def sizeV = n
                def e(no: Int) = g(no).toIterator
                def src = s
                def infinity = Int.MaxValue
            }
            for (i <- 1 to q) {
                val d = sp.dis(readInt)
                if (d == Int.MaxValue) println("Impossible")
                else println(d)
            }
            println
        }
    }

    // Snippet: ShortPath
    abstract class ShortPath[T: reflect.ClassTag](implicit num: Numeric[T]) {
        import collection._
        import collection.mutable.Buffer
        import num._

        def sizeV: Int
        def e(no: Int): Iterator[(Int, T)]
        def src: Int
        def infinity: T

        val dis = Array.fill(sizeV){infinity}
        val from = Array.ofDim[Int](sizeV)

        {
            val q = new java.util.ArrayDeque[Int]
            val inQ = Array.ofDim[Boolean](sizeV)
            dis(src) = zero
            q.add(src)
            inQ(src) = true
            while (!q.isEmpty) {
                val qH = q.poll
                inQ(qH) = false
                for ((t, c) <- e(qH)) {
                    val newDis = dis(qH) + c
                    if (newDis < dis(t)) {
                        dis(t) = newDis
                        from(t) = qH
                        if (!inQ(t)) {
                            if (!q.isEmpty && newDis < dis(q.peek))
                                q.addFirst(t)
                            else
                                q.addLast(t)
                            inQ(t) = true
                        }
                    }
                }
            }
        }

        def route(dst: Int) = {
            val ans = Buffer[Int]()
            var no = dst
            while (no != src) {
                ans.append(no)
                no = from(no)
            }
            ans.append(src)
            ans.reverse
        }
    }
}
