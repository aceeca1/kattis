// Algorithm: None
object erase {
    import io.StdIn._

    def main(args: Array[String]) {
        val n = readInt
        val s1 = readLine
        val s2 = readLine
        val ans =
            if ((n & 1) == 0) s1 == s2
            else s1.indices.forall(i => (s1(i) ^ s2(i)) == 1)
        if (ans) println("Deletion succeeded")
        else println("Deletion failed")
    }
}
