// Algorithm: Dynamic Programming
object ninepacks {
    import io.StdIn._

    def minItemGivenSum(a: Array[Int]) = {
        val ret = Array.fill(a.sum + 1){Int.MaxValue >> 1}
        ret(0) = 0
        for (i <- a)
            for (j <- Range(ret.size - 1, i - 1, -1))
                ret(j) = ret(j) min ret(j - i) + 1
        ret
    }

    def main(args: Array[String]) {
        val a1 = readLine.split(" ").map{_.toInt}.tail
        val a2 = readLine.split(" ").map{_.toInt}.tail
        if (a1.size == 0 || a2.size == 0) { println("impossible"); return }
        val b1 = minItemGivenSum(a1)
        val b2 = minItemGivenSum(a2)
        val b = Array.tabulate(b1.size min b2.size){i => b1(i) + b2(i)}
        val ans = b.tail.min
        if (ans < (Int.MaxValue >> 1)) println(ans)
        else println("impossible")
    }
}
