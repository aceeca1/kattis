// Algorithm: None
object halfacookie {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val line = readLine
            if (line == null) return
            val Array(r, x, y) = line.split(" ").map{_.toDouble}
            val h = Math.hypot(x, y)
            if (h > r) println("miss")
            else {
                val theta = Math.acos(h / r)
                val s1 = r * r * theta
                val s2 = r * r * Math.sin(theta * 2) * 0.5
                val sSmall = s1 - s2
                val sLarge = r * r * Math.PI - sSmall
                println(s"$sLarge $sSmall")
            }
        }
    }
}
