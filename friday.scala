// Algorithm: None
object friday {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            val Array(d, m) = readLine.split(" ").map{_.toInt}
            val a = readLine.split(" ").map{_.toInt}
            var first = 0
            var ans = 0
            for (i <- a) {
                if (i >= 13 && (first + 12) % 7 == 5) ans += 1
                first += i
            }
            println(ans)
        }
    }
}
