// Algorithm: Shortest Path
object getshorty {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        while (true) {
            val Array(n, m) = readLine.split(" ").map{_.toInt}
            if (n == 0) return
            val g = Array.fill(n){Buffer[(Int, Double)]()}
            for (_ <- 1 to m) {
                val line = readLine.split(" ")
                val n1 = line(0).toInt
                val n2 = line(1).toInt
                val c = line(2).toDouble
                g(n1).append((n2, -Math.log(c)))
                g(n2).append((n1, -Math.log(c)))
            }
            println(Math.exp(-new ShortPath[Double] {
                def sizeV = g.size
                def e(no: Int) = g(no).toIterator
                def src = 0
                def infinity = Double.PositiveInfinity
            }.dis(n - 1)).formatted("%.4f"))
        }
    }

    // Snippet: ShortPath
    abstract class ShortPath[T: reflect.ClassTag](implicit num: Numeric[T]) {
        import collection._
        import collection.mutable.Buffer
        import num._

        def sizeV: Int
        def e(no: Int): Iterator[(Int, T)]
        def src: Int
        def infinity: T

        val dis = Array.fill(sizeV){infinity}
        val from = Array.ofDim[Int](sizeV)

        {
            val q = new java.util.ArrayDeque[Int]
            val inQ = Array.ofDim[Boolean](sizeV)
            dis(src) = zero
            q.add(src)
            inQ(src) = true
            while (!q.isEmpty) {
                val qH = q.poll
                inQ(qH) = false
                for ((t, c) <- e(qH)) {
                    val newDis = dis(qH) + c
                    if (newDis < dis(t)) {
                        dis(t) = newDis
                        from(t) = qH
                        if (!inQ(t)) {
                            if (!q.isEmpty && newDis < dis(q.peek))
                                q.addFirst(t)
                            else
                                q.addLast(t)
                            inQ(t) = true
                        }
                    }
                }
            }
        }

        def route(dst: Int) = {
            val ans = Buffer[Int]()
            var no = dst
            while (no != src) {
                ans.append(no)
                no = from(no)
            }
            ans.append(src)
            ans.reverse
        }
    }
}
