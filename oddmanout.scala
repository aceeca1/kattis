// Algorithm: None
object oddmanout {
    import io.StdIn._

    def main(args: Array[String]) {
        for (cases <- 1 to readInt) {
            val n = readInt
            val a = readLine.split(" ").map{_.toInt}
            val ans = a.reduce(_ ^ _)
            println(s"Case #$cases: $ans")
        }
    }
}
