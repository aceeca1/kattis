// Algorithm: Greedy
object airconditioned {
    import io.StdIn._
    import collection.mutable.Buffer

    def main(args: Array[String]) {
        val a = Array.fill(readInt){readLine.split(" ").map{_.toInt}}
        val a1 = a.sortBy{ai => (ai(1), -ai(0))}
        val a2 = Buffer[Array[Int]]()
        for (i <- a1) if (a2.isEmpty || i(0) > a2.last(0)) a2.append(i)
        var ans = 0
        var i = 0
        while (i < a2.size) {
            val k = a2(i)(1)
            while (i < a2.size && a2(i)(0) <= k) i += 1
            ans += 1
        }
        println(ans)
    }
}
