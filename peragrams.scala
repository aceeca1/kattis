// Algorithm: None
object peragrams {
    import io.StdIn._

    def main(args: Array[String]) {
        val a = Array.ofDim[Int](256)
        for (i <- readLine) a(i) += 1
        val odd = a.count{i => (i & 1) != 0}
        println(if (odd == 0) 0 else odd - 1)
    }
}
