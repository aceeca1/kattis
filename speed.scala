// Algorithm: Binary Search
object speed {
    import io.StdIn._

    final def bSearch(s: Double, t: Double)(f: Double => Boolean): Double = {
        val mid = (s + t) * 0.5
        val err = 1e-7 * (Math.abs(mid) min 1.0)
        if (t - s < err) mid
        else if (f(mid)) bSearch(s, mid)(f)
        else bSearch(mid, t)(f)
    }

    def bSearch(s: Double)(f: Double => Boolean): Double = {
        var len = 1.0
        while (!f(s + len)) len += len
        bSearch(s, s + len)(f)
    }

    def main(args: Array[String]) {
        val Array(n, t) = readLine.split(" ").map{_.toInt}
        val d = Array.ofDim[Int](n)
        val s = Array.ofDim[Int](n)
        for (i <- 0 until n) {
            val line = readLine.split(" ").map{_.toInt}
            d(i) = line(0)
            s(i) = line(1)
        }
        println(bSearch(-s.min){c =>
            (0 until n).map{i => d(i) / (s(i) + c)}.sum < t
        })
    }
}
