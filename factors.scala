// Algorithm: Deep First Search
object factors {
    import io.StdIn._
    import collection._

    val prime = new Prime(100).toBuffer

    def solve(u: mutable.HashMap[Long, Int], ans: Array[Long]) {
        val n = u.keys.max

        def multiComb(a: Int, prev: Long, sum: Int): Long = {
            var ans = BigInt(prev)
            for (i <- 1 to a) {
                ans = ans * (sum - a + i) / i
                if (ans > Long.MaxValue) return -1
            }
            ans.toLong
        }

        def primePow(a: Int, prev: Long, len: Int): Long = {
            val ans = prev * BigInt(prime(len - 1)).pow(a)
            if (ans > Long.MaxValue) return -1
            ans.toLong
        }

        def put(a: Int, pA: Int, pLen: Int, pSum: Int, pMc: Long, pPp: Long) {
            val len = pLen + 1
            val sum = pSum + a
            val mc = multiComb(a, pMc, sum)
            if (mc == -1) return
            val pp = primePow(a, pPp, len)
            if (pp == -1) return
            if (mc > n) return
            u.get(mc) match {
                case Some(m) => if (pp < ans(m)) ans(m) = pp
                case None => ()
            }
            put(1, a, len, sum, mc, pp)
            if (a < pA) put(a + 1, pA, pLen, pSum, pMc, pPp)
        }

        put(1, Int.MaxValue, 0, 0, 1L, 1L)
        u.get(1) match {
            case Some(m) => ans(m) = 2L
            case None => ()
        }
    }

    def readNumbers: mutable.HashMap[Long, Int] = {
        val u = new mutable.HashMap[Long, Int]
        while (true) {
            val line = readLine
            if (line == null) return u
            u.update(line.toLong, u.size)
        }; null
    }

    def main(args: Array[String]) {
        val u = readNumbers
        val in = Array.ofDim[Long](u.size)
        val ans = Array.fill(u.size){Long.MaxValue}
        for ((k, v) <- u) in(v) = k
        solve(u, ans)
        for (i <- ans.indices) println("%d %d".format(in(i), ans(i)))
    }

    // Snippet: Prime
    class Prime(n: Int) {
        import collection.mutable.Buffer

        val a = Array.ofDim[Int](n + 1)

        {
            var u = 0
            for (i <- 2 to n) {
                var v = a(i)
                if (v == 0) { v = i; a(u) = i; u = i }
                var w = 2
                while (w > 0 && i * w <= n) {
                    a(i * w) = w
                    if (w >= v) w = -1 else w = a(w)
                }
            }
            a(u) = n + 1
        }

        def is(n: Int) = n >= 2 && a(n) > n
        def next(n: Int) = a(n)

        def toBuffer = {
            val ret = Buffer[Int]()
            var i = 2
            while (i <= n) {
                ret.append(i)
                i = a(i)
            }
            ret
        }
    }
}
