// Algorithm: None
object averageseasy {
    import io.StdIn._

    def main(args: Array[String]) {
        for (_ <- 1 to readInt) {
            readLine
            val Array(n1, n2) = readLine.split(" ").map{_.toInt}
            val a1 = readLine.split(" ").map{_.toInt}
            val a2 = readLine.split(" ").map{_.toInt}
            val avg1upper = (a1.sum + a1.size - 1) / a1.size
            val avg2lower = a2.sum / a2.size
            println(a1.count{i => avg2lower < i && i < avg1upper})
        }
    }
}
