// Algorithm: None
object boundingrobots {
    import io.StdIn._

    def main(args: Array[String]) {
        while (true) {
            val Array(w, l) = readLine.split(" ").map{_.toInt}
            if (w == 0) return
            var factX = 0
            var factY = 0
            var idealX = 0
            var idealY = 0
            for (_ <- 1 to readInt) {
                val line = readLine.split(" ")
                val len = line(1).toInt
                line(0) match {
                    case "r" =>
                        idealX += len
                        factX = factX + len min w - 1
                    case "u" =>
                        idealY += len
                        factY = factY + len min l - 1
                    case "l" =>
                        idealX -= len
                        factX = factX - len max 0
                    case "d" =>
                        idealY -= len
                        factY = factY - len max 0
                }
            }
            println(s"Robot thinks $idealX $idealY")
            println(s"Actually at $factX $factY")
            println
        }
    }
}
