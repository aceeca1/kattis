// Algorithm: None
object sortofsorting {
    import io.StdIn._

    def main(args: Array[String]) {
        var head = true
        while (true) {
            val n = readInt
            if (n == 0) return
            val a = Array.fill(n){readLine}.sortBy{ai => (ai(0), ai(1))}
            if (!head) println
            head = false
            for (i <- a) println(i)
        }
    }
}
