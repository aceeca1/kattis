// Algorithm: Lowest Common Ancestor
object collatz {
    import io.StdIn._
    import collection._
    import collection.mutable.Buffer
    
    def f(n: Long) = if ((n & 1L) != 0L) n * 3 + 1 else n >>> 1
    
    def goto1(n: Long) = {
        var nNow = n
        val ret = Buffer[Long]()
        while (nNow != 1) {
            ret.append(nNow)
            nNow = f(nNow)
        }
        ret.append(1)
        ret
    }
    
    def main(args: Array[String]) {
        while (true) {
            val Array(a, b) = readLine.split(" ").map{_.toLong}
            if (a == 0) return
            val aRoad = goto1(a)
            val bRoad = goto1(b)
            def f(sa: Int, sb: Int): (Int, Int) = {
                if (sa < 0 || sb < 0 || aRoad(sa) != bRoad(sb)) 
                    return (sa + 1, sb + 1)
                f(sa - 1, sb - 1)
            }
            var (sa, sb) = f(aRoad.size - 1, bRoad.size - 1)
            val c = aRoad(sa)
            println(s"$a needs $sa steps, $b needs $sb steps, they meet at $c")
        }
    }
}
